export function formatter(): string {
  const currentDate = new Date();
  const num: number = currentDate.getMonth() + 1;
  const num2: string = currentDate.getFullYear().toString();
  const num3: string = num2.slice(Math.ceil(num2.length / 2));

  const num4: string =
    num < 10
      ? num.toString().concat("0").split("").reverse().join("")
      : num.toString();

  return num4.concat(num3);
}
