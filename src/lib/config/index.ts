export class Config {
  public dbDatabase = process.env.DB_DATABASE;
  public dbHost = process.env.DB_HOST;
  public dbPassword = process.env.DB_PASSWORD;
  public dbType = process.env.DB_TYPE as "mssql";
  public dbUsername = process.env.DB_USERNAME;
  public dbSchema = process.env.DB_SCHEMA;
  public dbMigrationsRun = process.env.MIGRATIONS_RUN !== "FALSE";
  public port = process.env.NODE_PORT || "3333";
  public firebaseServiceAccount = require("../../../test-88133-firebase-adminsdk-rrggj-7e865adfe7.json");
  public dbLogging = process.env.DB_LOGGING ? true : false;
}
