export class Tracer {
  public trace: boolean = false;
  public pentacleRequest: Array<{
    url: string;
    method: string;
    payload: any;
  }> = [];
  constructor(allowTrace: boolean, traceMode: string = "false") {
    if (!allowTrace) {
      this.trace = false;
    } else {
      if (traceMode === "true") {
        this.trace = true;
      } else {
        this.trace = false;
      }
    }
  }
  public format() {
    return {
      pentacleRequest: this.pentacleRequest,
    };
  }
}
