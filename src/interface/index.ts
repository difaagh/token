import { Request } from "koa";
import { IMiddleware, IRouterContext } from "koa-router";
import { PartialResponsify, ResponseFormat } from "partial-responsify";
import {
  AppTypeOrm,
  BodyFormat,
  Config,
  ValidatorHeaderParam,
  ValidatorPathParam,
  Validator,
  Helper,
  PageableGetParam,
} from "../lib";
import {
  TokenEntity,
  TokenTransactionEntity,
  TokenTransactionConfigEntity,
  AddressBookEntity,
  TemplateChannelEntity,
} from "../entity";
import { Repository } from "typeorm";
import { TokenStatus } from "../enum";
import NotificationRequest from "../dto/NotificationRequest";
import { TemplateEntity } from "../entity/template";
import MergeResult from "../dto/MergeResult";
import { NotificationDto } from "../dto/NotificationDto";
export interface IDI {
  helper: Helper;
  validator: Validator;
  appTypeOrm: AppTypeOrm;
  config: Config;
  partialResponsify: PartialResponsify;
}
export interface IOpenApiRoute {
  bodyFormat?: BodyFormat;
  description?: string;
  path: string;
  headerParams?: ValidatorHeaderParam[];
  pathParams?: ValidatorPathParam[];
  method: "GET" | "POST" | "PUT" | "PATCH" | "DELETE";
  get?: ValidatorPathParam[];
  tags?: string;
  successResponseFormat?: ResponseFormat;
}

export interface IAllRoute {
  path: string;
  tmpPath?: string;
  method: "GET" | "POST" | "PUT" | "PATCH" | "DELETE";
  func: IMiddleware;
}

export interface IKoaRequestWithBody extends IRouterContext {
  request: IKoaBodyParserRequest;
}

export interface IKoaBodyParserRequest extends Request {
  body: any;
  app: any;
  req: any;
  res: any;
  ctx: any;
  response: any;
  originalUrl: any;
  ip: any;
  accept: any;
  charset: any;
  length: any;
  type: any;
  inspect: any;
  toJSON: any;
  header: any;
  headers: any;
  url: any;
  origin: any;
  path: any;
  href: any;
  method: any;
  query: any;
  querystring: any;
  search: any;
  host: any;
  hostname: any;
  URL: any;
  fresh: any;
  stale: any;
  idempotent: any;
  socket: any;
  protocol: any;
  secure: any;
  ips: any;
  subdomains: any;
  accepts: any;
  acceptsEncodings: any;
  acceptsCharsets: any;
  acceptsLanguages: any;
  is: any;
  get: any;
  rawBody: any;
}

export interface TokenServiceInterface {
  registerToken(userId: string, mobileNo: string): Promise<void>;

  findByUserIdAndTokenStatusAndDeleteFlagIsFalse(
    userId: string,
    tokenStatus: TokenStatus
  ): Promise<TokenEntity>;

  getTokenByUserId(userId: string): Promise<TokenEntity>;

  generateReferenceNo(bulk: number): Promise<number>;
}
export interface TokenTransactionInterface {
  create(data: any): Promise<TokenTransactionEntity>;

  findByRefNoAndDeleteFlagIsFalse(
    refNo: string
  ): Promise<TokenTransactionEntity>;
}
export interface TokenTransactionConfigInterface {
  findByTrxCodeAndDeleteFlagIsFalse(
    trxCode: string
  ): Promise<TokenTransactionConfigEntity>;

  getTokenTransactionConfigurationByTrxCode(
    trxCode: string
  ): Promise<TokenTransactionConfigEntity>;
}
export interface AddressBookInterface {
  getAddressBook(userId: string, channel: string): Promise<AddressBookEntity[]>;
  findByUserId(userId: string): Promise<AddressBookEntity[]>;
  findByUserIdAndChannel(
    userId: string,
    channel: string
  ): Promise<AddressBookEntity[]>;
  findByDeviceIdAndChannel(
    deviceId: string,
    channel: string
  ): Promise<AddressBookEntity>;
  create(addressBook: AddressBookEntity): void;
  deleteByUserId(userId: string): void;
  deleteByUserIdAndChannel(userId: string, channel: string): void;
}
export interface NotificationInterface {
  send(notificationRequest: NotificationRequest): void;
  sendWithTemplate(notificationRequest: NotificationRequest): void;
  sendWithTemplates(
    notificationRequest: NotificationRequest,
    channel: string
  ): void;
  subscribe(addressBook: AddressBookEntity): void;
  unsubscribe(userId: string): void;
}
export interface NotificationEngine {
  subscribe(
    userId: string,
    deviceType: string,
    deviceId: string
  ): Promise<string>;
  unsubscribe(address: string): Promise<void>;
  sendNotification(notificationDto: NotificationDto): Promise<string>;
}
export interface TemplateChannelInterface {
  findByTemplateCodeAndChannel(
    templateCode: string,
    channel: string
  ): Promise<TemplateChannelEntity>;
  findByTemplateCode(templateCode: string): Promise<TemplateChannelEntity[]>;
}
export interface TemplateMergeInterface {
  merge(
    templateCode: string,
    channel: string,
    valueMap: any
  ): Promise<MergeResult>;
  merges(templateCode: string, valueMap: any): Promise<MergeResult[]>;
}
export interface TemplateInterface {
  findByCode(code: string): Promise<TemplateEntity>;
  findSearch(
    // code: string,
    // name: string,
    search: PageableGetParam
  ): Promise<TemplateEntity[]>;
  update(newTemplate: any): Promise<void>;
}
