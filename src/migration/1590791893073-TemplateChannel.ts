import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class TemplateChannel1590791893073 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: "not_template_channel",
        columns: [
          {
            name: "id",
            type: "UNIQUEIDENTIFIER",
            isGenerated: true,
            isPrimary: true,
            generationStrategy: "uuid",
          },
          {
            name: "created_by",
            type: "char",
            length: "40",
            isNullable: true,
            default: null,
          },
          {
            name: "template_cd",
            type: "char",
            length: "40",
            isNullable: true,
          },
          {
            name: "date_created",
            type: "datetime",
            default: null,
            isNullable: true,
          },
          {
            name: "last_updated",
            type: "datetime",
            default: null,
            isNullable: true,
          },
          {
            name: "updated_by",
            type: "char",
            length: "40",
            isNullable: true,
            default: null,
          },
          {
            name: "version",
            type: "int",
            isNullable: true,
            default: null,
          },
          {
            name: "subject",
            type: "varchar",
            length: "100",
            isNullable: true,
            default: null,
          },
          {
            name: "channel",
            type: "varchar",
            length: "40",
            isNullable: true,
          },
          {
            name: "content",
            type: "text",
            isNullable: true,
          },
          {
            name: "inactive_flag",
            type: "bit",
            isNullable: true,
            default: null,
          },
        ],
      })
    );
    // .then(async () => {
    //   await queryRunner.query(
    //     `CREATE INDEX [FKs5qriwct3dw4bg88g0xvtadkc] ON [not_template_channel] ([template_cd])`
    //   );
    //   await queryRunner.query(
    //     `alter table [not_template_channel] add constraint FKs5qriwct3dw4bg88g0xvtadkc foreign key (template_cd) references [not_template] (code);`
    //   );
    // });
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    return null;
  }
}
