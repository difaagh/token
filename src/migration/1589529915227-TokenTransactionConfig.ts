import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class TokenTransactionConfig1589529915227 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: "mnt_token_transaction_config",
        columns: [
          {
            name: "id",
            type: "nvarchar",
            isPrimary: true,
            length: "100",
          },
          {
            name: "index",
            type: "int",
          },
          {
            name: "name",
            type: "varchar",
            length: "40",
          },
          {
            name: "trx_code",
            type: "nvarchar",
            length: "40",
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    return null;
  }
}
