import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class Token1589529926882 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: "mnt_token",
        columns: [
          {
            name: "id",
            type: "nvarchar",
            isPrimary: true,
          },
          {
            name: "index",
            type: "int",
            // length: "999",
          },
          {
            name: "mobile_phone_no",
            type: "varchar",
            length: "20",
          },
          {
            name: "is_otp_token",
            type: "bit",
            default: "0",
          },
          {
            name: "no_of_invalid",
            type: "int",
          },
          {
            name: "serial_no",
            type: "varchar",
            length: "40",
          },
          {
            name: "token_status",
            type: "nvarchar",
            length: "100",
          },
          {
            name: "delete_flag",
            type: "bit",
            default: "0",
          },
          {
            name: "user_id",
            type: "nvarchar",
            length: "100",
          },
          {
            name: "date_created",
            type: "datetimeoffset",
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    return null;
  }
}
