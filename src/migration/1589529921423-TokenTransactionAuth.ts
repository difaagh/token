import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class TokenTransactionAuth1589529921423 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: "mnt_token_transaction_auth",
        columns: [
          {
            name: "id",
            type: "nvarchar",
            isPrimary: true,
            length: "100",
          },
          {
            name: "delete_flag",
            type: "bit",
            default: "0",
          },
          {
            name: "description",
            type: "varchar",
            length: "100",
          },
          {
            name: "index",
            type: "int",
          },
          {
            name: "name",
            type: "varchar",
            length: "40",
          },
          {
            name: "no_of_factor",
            type: "int",
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    return null;
  }
}
