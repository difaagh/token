import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class AddressBook1591214086667 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: "not_addr_book",
        columns: [
          {
            name: "id",
            type: "UNIQUEIDENTIFIER",
            isGenerated: true,
            isPrimary: true,
            generationStrategy: "uuid",
          },
          {
            name: "created_by",
            type: "char",
            length: "40",
            isNullable: true,
            default: null,
          },
          {
            name: "address",
            type: "varchar",
            length: "500",
            isNullable: false,
          },
          {
            name: "date_created",
            type: "datetime",
            default: null,
            isNullable: true,
          },
          {
            name: "device_id",
            type: "varchar",
            length: "500",
            isNullable: true,
            default: null,
            isUnique: true,
          },
          {
            name: "device_typ",
            type: "varchar",
            length: "40",
            isNullable: true,
            default: null,
            isUnique: true,
          },
          {
            name: "channel",
            type: "varchar",
            length: "40",
            isNullable: true,
          },
          {
            name: "user_id",
            type: "char",
            isNullable: false,
            length: "40",
            isUnique: true,
          },
          {
            name: "delete_flag",
            type: "bit",
            isNullable: true,
            default: null,
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    return null;
  }
}
