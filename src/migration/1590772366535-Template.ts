import { MigrationInterface, QueryRunner } from "typeorm";

export class Template1590772366535 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    //     .query(
    //       `DROP TABLE IF EXISTS [not_template_channel];CREATE TABLE [not_template_channel] (
    // [id] char(255) NOT NULL,
    // [created_by] char(40) DEFAULT NULL,
    // [date_created] datetime2 DEFAULT NULL,
    // [last_updated] datetime2 DEFAULT NULL,
    // [updated_by] char(40) DEFAULT NULL,
    // [version] int DEFAULT NULL,
    // [channel] varchar(40) DEFAULT NULL,
    // [content] varchar(max),
    // [inactive_flag] bit DEFAULT NULL,
    // [subject] varchar(100) DEFAULT NULL,
    // [template_cd] char(40) DEFAULT NULL,
    // PRIMARY KEY ([id])
    //   );`
    //     )
    // .then(async () => {}
    //   await queryRunner
    //     .query(
    //       `DROP TABLE IF EXISTS [not_addr_book];CREATE TABLE [not_addr_book] (
    // [id] char(40) NOT NULL,
    // [created_by] char(40) DEFAULT NULL,
    // [date_created] datetime2 DEFAULT NULL,
    // [address] varchar(500) NOT NULL,
    // [channel] char(40) NOT NULL,
    // [device_id] varchar(500) DEFAULT NULL,
    // [device_typ] varchar(40) DEFAULT NULL,
    // [user_id] char(40) NOT NULL,
    // [delete_flag] bit DEFAULT NULL,
    // PRIMARY KEY ([id]),CONSTRAINT [UKa0ni7ei6vfe60bo49giryf7oh] UNIQUE  ([user_id],[device_typ],[device_id]));`
    // )
    // ;}
    // )
    // .then(async () => {}
    await queryRunner
      .query(
        `DROP TABLE IF EXISTS [not_template_parameter];CREATE TABLE [not_template_parameter](
  [template_code] char(40) NOT NULL,
  [name] varchar(100) DEFAULT NULL,
  [identifier] char(40) NOT NULL,
  PRIMARY KEY ([template_code],[identifier]));`
      )
      .then(async () => {
        await queryRunner.query(`DROP TABLE IF EXISTS [not_template];CREATE TABLE [not_template] (
  [code] char(40) NOT NULL,
  [created_by] char(40) DEFAULT NULL,
  [date_created] datetime2 DEFAULT NULL,
  [last_updated] datetime2 DEFAULT NULL,
  [updated_by] char(40) DEFAULT NULL,
  [version] int DEFAULT NULL,
  [delete_flag] bit DEFAULT NULL,
  [description] varchar(500) DEFAULT NULL,
  [inactive_flag] bit DEFAULT NULL,
  [idx] int NOT NULL,
  [name] varchar(100) NOT NULL,
  PRIMARY KEY ([code])
);`);
      });
    // .then(async () => {
    // await queryRunner.query(
    //   `CREATE INDEX [FKs5qriwct3dw4bg88g0xvtadkc] ON [not_template_channel] ([template_cd])`
    // );
    // await queryRunner.query(
    //   `alter table [not_template_channel] add constraint FKs5qriwct3dw4bg88g0xvtadkc foreign key (template_cd) references [not_template] (code);`
    // );
    //   await queryRunner.query(
    //     `CREATE INDEX [FKjew30seqr55m44m1ipp3t39qw] ON [not_template_parameter] ([template_code])`
    //   );
    //   await queryRunner.query(
    //     `alter table [not_template_parameter] add constraint FKjew30seqr55m44m1ipp3t39qw foreign key (template_code) references [not_template] (code);`
    //   );
    // });
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    return null;
  }
}
