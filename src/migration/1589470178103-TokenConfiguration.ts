import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class TokenConfiguration1589470178103 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: "mnt_token_configuration",
        columns: [
          {
            name: "id",
            type: "nvarchar",
            isPrimary: true,
            length: "100",
          },
          {
            name: "index",
            type: "nvarchar",
            length: "100",
          },
          {
            name: "max_invalid_token",
            type: "int",
          },
          {
            name: "validity",
            type: "int",
          },
          {
            name: "token_length",
            type: "nvarchar",
            length: "100",
          },
          {
            name: "delete_flag",
            type: "bit",
            default: "0",
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    return null;
  }
}
