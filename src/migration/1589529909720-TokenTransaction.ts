import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class TokenTransaction1589529909720 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: "mnt_token_transaction",
        columns: [
          {
            name: "id",
            type: "UNIQUEIDENTIFIER",
            isGenerated: true,
            isPrimary: true,
            generationStrategy: "uuid",
          },
          {
            name: "expired_date",
            type: "datetimeoffset",
            isNullable: true,
          },
          {
            name: "factor_1",
            type: "varchar",
            length: "10",
          },
          {
            name: "factor_2",
            type: "varchar",
            length: "10",
          },
          {
            name: "factor_3",
            type: "varchar",
            length: "10",
          },
          {
            name: "factor_4",
            type: "varchar",
            length: "10",
          },
          {
            name: "factor_5",
            type: "varchar",
            length: "10",
          },
          {
            name: "factor_6",
            type: "varchar",
            length: "10",
          },
          {
            name: "factor_7",
            type: "varchar",
            length: "10",
          },
          {
            name: "factor_8",
            type: "varchar",
            length: "10",
          },
          {
            name: "factor_9",
            type: "varchar",
            length: "10",
          },
          {
            name: "factor_10",
            type: "varchar",
            length: "10",
          },
          {
            name: "index",
            type: "int",
            // length: "100",
          },
          {
            name: "ref_No",
            type: "varchar",
            length: "10",
          },
          {
            name: "delete_flag",
            type: "bit",
            default: "0",
          },
          {
            name: "is_used",
            type: "char",
            length: "1",
          },
          {
            name: "date_created",
            type: "datetimeoffset",
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    return null;
  }
}
