import { Validator } from "le-validator";
import { PartialResponsify } from "partial-responsify";
import { AppTypeOrm, Config, Helper } from "../lib";
import { TokenConfiguration1589470178103 } from "../migration/1589470178103-TokenConfiguration";
import { TokenTransactionConfig1589529915227 } from "../migration/1589529915227-TokenTransactionConfig";
import { TokenTransactionAuth1589529921423 } from "../migration/1589529921423-TokenTransactionAuth";
import { Token1589529926882 } from "../migration/1589529926882-Token";
import { TokenTransaction1589529909720 } from "../migration/1589529909720-TokenTransaction";
import { AddressBook1591214086667 } from "../migration/1591214086667-AddressBook";
import { TemplateChannel1590791893073 } from "../migration/1590791893073-TemplateChannel";
// import { TemplateParameter1590659639537 } from "../migration/1590659639537-TemplateParameter";
import { Template1590772366535 } from "../migration/1590772366535-Template";
import { IDI } from "../interface";
import {
  TokenTransactionConfigEntity,
  TokenConfigEntity,
  TokenTransactionAuthEntity,
  TokenEntity,
  TokenTransactionEntity,
  AddressBookEntity,
  TemplateChannelEntity,
  TemplateEntity,
  TemplateParameterEntity,
} from "../entity";

export const getAppTypeOrm = async (
  config: Config,
  extraConfig: {
    withMigration: boolean;
  }
): Promise<AppTypeOrm> => {
  const migrations = extraConfig.withMigration
    ? [
        AddressBook1591214086667,
        TokenConfiguration1589470178103,
        TokenTransactionConfig1589529915227,
        TokenTransactionAuth1589529921423,
        Token1589529926882,
        TokenTransaction1589529909720,
        Template1590772366535,
        TemplateChannel1590791893073,
        // TemplateParameter1590659639537,
      ]
    : [];
  return new AppTypeOrm(
    await AppTypeOrm.getConnectionManager({
      database: config.dbDatabase,
      entities: [
        AddressBookEntity,
        TemplateEntity,
        TokenConfigEntity,
        TokenTransactionConfigEntity,
        TokenTransactionAuthEntity,
        TokenEntity,
        TokenTransactionEntity,
        TemplateParameterEntity,
        TemplateChannelEntity,
      ],
      host: config.dbHost,
      // logging: config.dbLogging,
      migrations,
      migrationsRun: config.dbMigrationsRun,
      options: {
        encrypt: true,
        enableArithAbort: false,
      },
      password: config.dbPassword,
      // pool: {
      //     max: config.dbMaxPool,
      //     min: 0,
      // },
      schema: config.dbSchema,
      type: config.dbType,
      username: config.dbUsername,
      logging: config.dbLogging ? ["query"] : false,
    })
  );
};

export const getDiComponent = async (config: Config): Promise<IDI> => {
  const appTypeOrm = await getAppTypeOrm(config, {
    withMigration: true,
  });
  console.log(config);
  await appTypeOrm.runMigration();
  const theDi: any = {
    helper: new Helper(),
    config,
    partialResponsify: new PartialResponsify(),
    validator: new Validator(),
    appTypeOrm,
  };
  return theDi;
};
