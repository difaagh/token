export default class NotificationRequest {
  userId: string;

  channel: string;

  subject: string;

  message: string;

  templateCode: string;

  valueMap: any;

  constructor() {
    if (this.userId === undefined) this.userId = null;
    if (this.channel === undefined) this.channel = null;
    if (this.subject === undefined) this.subject = null;
    if (this.message === undefined) this.message = null;
    if (this.templateCode === undefined) this.templateCode = null;
    this.valueMap = <any>{};
  }
}
