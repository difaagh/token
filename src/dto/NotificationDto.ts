export class NotificationDto {
  userId: string;
  channel: string;
  deviceType: string;
  to: string;
  subject: string;
  message: string;
  valueMap: any;
  constructor(
    userId: string,
    channel: string,
    deviceType: string,
    to: string,
    subject: string,
    message: string,
    valueMap: any
  ) {
    if (userId !== null || userId !== undefined) {
      this.userId = userId;
    }
    if (channel !== null || channel !== undefined) {
      this.channel = channel;
    }
    if (deviceType !== null || deviceType !== undefined) {
      this.deviceType = deviceType;
    }
    if (to !== null || to !== undefined) {
      this.to = to;
    }
    if (subject !== null || subject !== undefined) {
      this.subject = subject;
    }
    if (message !== null || message !== undefined) {
      this.message = message;
    }
    if (valueMap !== null || valueMap !== undefined) {
      this.valueMap = valueMap;
    }
  }
}
