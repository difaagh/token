export default class MergeResult {
  channel: string;

  subject: string;

  /**
   */
  message: string;

  constructor(channel?: string, subject?: string, message?: string) {
    if (this.channel !== undefined || this.channel !== null) {
      this.channel = channel;
    }
    if (this.subject !== undefined || this.subject !== null) {
      this.subject = subject;
    }
    if (this.message !== undefined || this.message !== null) {
      this.message = message;
    }
  }
}
