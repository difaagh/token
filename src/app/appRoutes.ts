import * as bodyparser from "koa-bodyparser";
import * as Router from "koa-router";
import { logger } from "../middleware";
import { baseRoutes } from "./allRoutes/api/v1";
import { uptime } from "./allRoutes/uptime";
export class AppRoutes {
    private baseRouter: Router;
    constructor(needLogger: boolean) {
        const apiV1Router = new Router({
            prefix: "/api",
        });
        const apiRouter = new Router({
            prefix: "/api",
        });
        this.baseRouter = new Router();
        this.baseRouter.get("/", uptime);
        apiRouter.get("/", uptime);
        for (const baseRoute of baseRoutes) {
            const args: any[] = [];
            if (needLogger) {
                args.push(logger);
            }
           if (baseRoute.method === "GET") {
                args.push(baseRoute.func);
                apiV1Router.get(baseRoute.path, ...args);
                if (baseRoute.tmpPath) {
                    apiV1Router.get(baseRoute.tmpPath, ...args);
                }
            }
            if (baseRoute.method === "PUT") {
                args.push(bodyparser(), baseRoute.func);
                apiV1Router.put(baseRoute.path, ...args);
                if (baseRoute.tmpPath) {
                    apiV1Router.put(baseRoute.tmpPath, ...args);
                }
            }
            if (baseRoute.method === "POST") {
                args.push(bodyparser(), baseRoute.func);
                apiV1Router.post(baseRoute.path, ...args);
                if (baseRoute.tmpPath) {
                    apiV1Router.post(baseRoute.tmpPath, ...args);
                }
            }
            if (baseRoute.method === "PATCH") {
                args.push(bodyparser(), baseRoute.func);
                apiV1Router.patch(baseRoute.path, ...args);
                if (baseRoute.tmpPath) {
                    apiV1Router.patch(baseRoute.tmpPath, ...args);
                }
            }
            if (baseRoute.method === "DELETE") {
                args.push(bodyparser(), baseRoute.func);
                apiV1Router.delete(baseRoute.path, ...args);
                if (baseRoute.tmpPath) {
                    apiV1Router.delete(baseRoute.tmpPath, ...args);
                }
            }
        }
        this.baseRouter.use("", apiRouter.routes());
        this.baseRouter.use("", apiV1Router.routes());
    }
    public getRoutes() {
       return this.baseRouter.routes();
    }
    public getAllowedMethods() {
       return this.baseRouter.allowedMethods();
    }
}
