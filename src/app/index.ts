import { getApp } from "./app";

(async () => {
  try {
    const { app, config } = await getApp();
    app.listen(config.port);
    // tslint:disable-next-line:no-console
    console.log(`Server running on port ${config.port}`);
  } catch (err) {
    // tslint:disable-next-line:no-console
    console.log(err);
    process.exit(1);
  }
})();
