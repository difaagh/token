import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../../interface";
import TemplateChannelService from "../../../../../../services/template/TemplateChannelService";
import {
  pageableGetParamValidator,
  PageableGetParam,
  ValidatorGetParam,
} from "../../../../../../lib";
import TemplateService from "../../../../../../services/template/TemplateService";
const path: string = "/v1/notification/templatechan";
const method = "GET";
const get: ValidatorGetParam[] = [
  ...pageableGetParamValidator,
  {
    minLength: 1,
    name: "fields",
    required: false,
    swagger: {
      description: "Fields needed from response",
      example: "",
    },
    type: "string",
    default:
      "description,code,dateCreated,createdBy,lastUpdated,updatedBy,version,idx,name",
  },
];
const responseFormat: ResponseFormat = {
  items: {
    fields: {
      description: {
        type: "any",
      },
      code: {
        type: "any",
      },
      dateCreated: {
        type: "any",
      },
      createdBy: {
        type: "any",
      },
      lastUpdated: {
        type: "any",
      },
      updatedBy: {
        type: "any",
      },
      version: {
        type: "any",
      },
      idx: {
        type: "any",
      },
      name: {
        type: "any",
      },
    },
    type: "object",
  },
  type: "array",
};
const successResponseFormat: ResponseFormat = {
  fields: {
    data: responseFormat,
  },
  type: "object",
};
const func = async (ctx: IRouterContext) => {
  const di: IDI = ctx.state.di;
  const query = di.validator.processQuery<
    PageableGetParam & { fields: string }
  >(get, ctx.query);
  const fieldsToUse = di.partialResponsify.parseFields(
    query.fields,
    responseFormat
  );
  const tokenService = new TemplateChannelService(di);
  const theMap: Map<string, string> = new Map([
    ["code", "code"],
    ["description", "description"],
    ["createdBy", "createdBy"],
    ["dateCreated", "dateCreated"],
    ["lastUpdated", "lastUpdated"],
    ["updatedBy", "updatedBy"],
    ["version", "version"],
    ["idx", "idx"],
    ["name", "name"],
  ]);
  // let result;
  // if (query.code.length === 0) {
  //   result = di.partialResponsify.parseResult(
  //     fieldsToUse,
  //     responseFormat,
  //     di.helper.restructureArrObject(
  //       await tokenService.findSearch(query),
  //       theMap
  //     )
  //   );
  // } else {
  const service = new TemplateService(di);
  const result = await tokenService.findByTemplateCode("code2");
  // }
  ctx.status = 200;
  ctx.body = { data: result };
};
export const openapiGetTemplateChan: IOpenApiRoute = {
  get,
  method,
  path,
  successResponseFormat,
  // tags: ["masterdata"],
};
export const getTemplateChan: IAllRoute = {
  func,
  method,
  path,
};
