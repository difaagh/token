import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../../interface";
import { ValidatorGetParam } from "../../../../../../lib";
import TemplateService from "../../../../../../services/template/TemplateService";

const path: string = "/v1/notification/template";
const method = "DELETE";
const get: ValidatorGetParam[] = [
  {
    minLength: 1,
    name: "code",
    required: true,
    swagger: {
      description: "Id of an entity",
      example: "",
    },
    type: "string",
  },
  {
    minLength: 1,
    name: "fields",
    required: false,
    swagger: {
      description: "Fields needed from response",
      example: "",
    },
    type: "string",
    default: "code",
  },
];
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat: ResponseFormat = {
  items: {
    fields: {
      code: {
        type: "any",
      },
    },
    type: "object",
  },
  type: "array",
};
const successResponseFormat: ResponseFormat = {
  fields: {
    data: responseFormat,
  },
  type: "object",
};
const func = async (ctx: IRouterContext) => {
  const di: IDI = ctx.state.di;
  const query = di.validator.processQuery<{ fields: string; code: string }>(
    get,
    ctx.query
  );
  const fieldsToUse = di.partialResponsify.parseFields(
    query.fields,
    responseFormat
  );
  // const theMap: Map<string, string> = new Map([
  // ["id", "id"],
  // ["index", "index"],
  // ["isOtpToken", "isOtpToken"],
  // ["noOfInvalid", "noOfInvalid"],
  // ["serialNo", "serialNo"],
  // ["tokenStatus", "tokenStatus"],
  //   ["code", "code"],
  // ]);
  const templateService = new TemplateService(di);
  try {
    await templateService.delete(query.code);
  } catch (e) {
    throw new Error(`erorr ${e}`);
  }
  // const result = di.partialResponsify.parseResult(
  //   fieldsToUse,
  //   responseFormat,
  //   di.helper.restructureArrObject([deleteTemplate], theMap)[0]
  // );
  ctx.status = 200;
  ctx.body = { data: "success to delete" };
};
export const openapiDeleteTemplate: IOpenApiRoute = {
  get,
  method,
  path,
  successResponseFormat,
};
export const deletedTemplate: IAllRoute = {
  func,
  method,
  path,
};
