import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../../interface";
import { ValidatorGetParam, BodyFormat } from "../../../../../../lib";
import TemplateService from "../../../../../../services/template/TemplateService";
interface IQuery {
  [key: string]: string;
}

const path: string = "/v1/notification/template";
const method = "PATCH";
const get: ValidatorGetParam[] = [
  {
    minLength: 1,
    name: "fields",
    required: false,
    swagger: {
      description: "Fields needed from response",
      example: "",
    },
    type: "string",
    default: "code,description,version,idx,name,innactiveFlag",
  },
];
const body: BodyFormat = {
  required: true,
  type: "object",
  fields: {
    code: {
      type: "string",
      minLength: 1,
      required: true,
    },
    description: {
      type: "string",
      minLength: 1,
      required: true,
    },
    version: {
      type: "string",
      minLength: 1,
      required: true,
    },
    idx: {
      type: "string",
      minLength: 1,
      required: true,
    },
    name: {
      type: "string",
      minLength: 1,
      required: true,
    },
    innactiveFlag: {
      type: "string",
      required: true,
    },
  },
};
const responseFormat: ResponseFormat = {
  fields: {
    code: {
      type: "any",
    },
    description: {
      type: "any",
    },
    version: {
      type: "any",
    },
    idx: {
      type: "any",
    },
    name: {
      type: "any",
    },
    innactiveFlag: {
      type: "any",
    },
  },
  type: "object",
};
const successResponseFormat: ResponseFormat = {
  fields: {
    data: responseFormat,
  },
  type: "object",
};
const func = async (ctx: IRouterContext) => {
  const di: IDI = ctx.state.di;
  console.log(ctx.request.body);
  const requestBody = di.validator.processBody<{
    code: string;
    description: string;
    version: number;
    idx: number;
    name: string;
    innactiveFlag: number;
  }>(body, ctx.request.body);
  const query = di.validator.processQuery<{ fields: string }>(get, ctx.query);
  const fieldsToUse = di.partialResponsify.parseFields(
    query.fields,
    responseFormat
  );
  const theMap: Map<string, string> = new Map([
    ["code", "code"],
    ["description", "description"],
    ["version", "version"],
    ["idx", "idx"],
    ["name", "name"],
    ["innactiveFlag", "innactiveFlag"],
  ]);
  const templateService = new TemplateService(di);
  console.log("disini");
  // const updatedTokenTransactionAuth = await templateService.update(requestBody);
  // const result = di.partialResponsify.parseResult(
  //   fieldsToUse,
  //   responseFormat,
  //   di.helper.restructureArrObject([updatedTokenTransactionAuth], theMap)[0]
  // );
  const process = await templateService
    .update(requestBody)
    .catch((err: any) => {
      if (err) {
        console.log(err);
        throw new Error(`error : ${err}`);
      }
    });
  ctx.status = 200;
  ctx.body = { data: "success to update template" };
};
export const openapiPatchTemplate: IOpenApiRoute = {
  get,
  method,
  path,
  successResponseFormat,
};
export const patchTemplate: IAllRoute = {
  func,
  method,
  path,
};
