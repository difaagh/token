import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../../interface";
// import { ValidatorGetParam, ValidatorHeaderParam } from "../../../../../lib";
// import {
//     commonHeaderParams,
//     ICommonHeaderParamsProcessed,
// } from "../../../../../scripts";
import TokenService from "../../../../../../services/token/TokenService";
import {
  pageableGetParamValidator,
  PageableGetParam,
  ValidatorGetParam,
} from "../../../../../../lib";
const path: string = "/v1/token";
const method = "GET";
const get: ValidatorGetParam[] = [
  ...pageableGetParamValidator,
  {
    minLength: 1,
    name: "fields",
    required: false,
    swagger: {
      description: "Fields needed from response",
      example: "",
    },
    type: "string",
    default:
      "id,index,isOtpToken,noOfInvalid,serialNo,tokenStatus,userId,mobilePhoneNo,dateCreated",
  },
];
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat: ResponseFormat = {
  items: {
    fields: {
      id: {
        type: "any",
      },
      index: {
        type: "any",
      },
      isOtpToken: {
        type: "any",
      },
      noOfInvalid: {
        type: "any",
      },
      serialNo: {
        type: "string",
      },
      tokenStatus: {
        type: "string",
      },
      userId: {
        type: "string",
      },
      mobilePhoneNo: {
        type: "string",
      },
      dateCreated: {
        type: "string",
      },
    },
    type: "object",
  },
  type: "array",
};
const successResponseFormat: ResponseFormat = {
  fields: {
    data: responseFormat,
  },
  type: "object",
};
const func = async (ctx: IRouterContext) => {
  const di: IDI = ctx.state.di;
  // const ddi: IDDI = ctx.state.ddi;
  // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);
  const query = di.validator.processQuery<
    PageableGetParam & { fields: string }
  >(get, ctx.query);
  const fieldsToUse = di.partialResponsify.parseFields(
    query.fields,
    responseFormat
  );
  const tokenService = new TokenService(di);
  const theMap: Map<string, string> = new Map([
    ["id", "id"],
    ["index", "index"],
    ["isOtpToken", "isOtpToken"],
    ["noOfInvalid", "noOfInvalid"],
    ["serialNo", "serialNo"],
    ["tokenStatus", "tokenStatus"],
    ["dateCreated", "dateCreated"],
  ]);
  const result = di.partialResponsify.parseResult(
    fieldsToUse,
    responseFormat,
    di.helper.restructureArrObject(await tokenService.findAll(query), theMap)
  );
  ctx.status = 200;
  ctx.body = { data: result };
};
export const openapiToken: IOpenApiRoute = {
  get,
  method,
  path,
  successResponseFormat,
  // tags: ["masterdata"],
};
export const getToken: IAllRoute = {
  func,
  method,
  path,
};
