import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../../interface";
// import { ValidatorGetParam, ValidatorHeaderParam } from "../../../../../lib";
// import {
//     commonHeaderParams,
//     ICommonHeaderParamsProcessed,
// } from "../../../../../scripts";
import TokenTransactionConfigService from "../../../../../../services/token/TokenTransactionConfigService";
import {
  pageableGetParamValidator,
  PageableGetParam,
  ValidatorGetParam,
} from "../../../../../../lib";
const path: string = "/v1/tokentransactionconfig";
const method = "GET";
const get: ValidatorGetParam[] = [
  ...pageableGetParamValidator,
  {
    minLength: 1,
    name: "fields",
    required: false,
    swagger: {
      description: "Fields needed from response",
      example: "",
    },
    type: "string",
    default: "id,index,name,trxCode",
  },
];
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat: ResponseFormat = {
  items: {
    fields: {
      id: {
        type: "string",
      },
      index: {
        type: "any",
      },
      name: {
        type: "string",
      },
      trxCode: {
        type: "string",
      },
      // tokenLength: {
      //   type: "string",
      // },
    },
    type: "object",
  },
  type: "array",
};
const successResponseFormat: ResponseFormat = {
  fields: {
    data: responseFormat,
  },
  type: "object",
};
const func = async (ctx: IRouterContext) => {
  const di: IDI = ctx.state.di;
  // const ddi: IDDI = ctx.state.ddi;
  // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);
  const query = di.validator.processQuery<
    PageableGetParam & { fields: string }
  >(get, ctx.query);
  const fieldsToUse = di.partialResponsify.parseFields(
    query.fields,
    responseFormat
  );
  const tokenTransactionConfig = new TokenTransactionConfigService(di);
  const theMap: Map<string, string> = new Map([
    ["id", "id"],
    ["index", "index"],
    ["name", "name"],
    ["trxCode", "trxCode"],
    ["tokenTransactionAuth", "tokenTransactionAuth"],
  ]);
  const result = di.partialResponsify.parseResult(
    fieldsToUse,
    responseFormat,
    di.helper.restructureArrObject(
      await tokenTransactionConfig.findAll(query),
      theMap
    )
  );
  ctx.status = 200;
  ctx.body = { data: result };
};
export const openapiGetTokenTransactionConfig: IOpenApiRoute = {
  get,
  method,
  path,
  successResponseFormat,
  // tags: ["masterdata"],
};
export const getTokenTransactionConfig: IAllRoute = {
  func,
  method,
  path,
};
