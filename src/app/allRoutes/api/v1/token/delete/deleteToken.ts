import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../../interface";
import { ValidatorGetParam } from "../../../../../../lib";
import TokenService from "../../../../../../services/token/TokenService";

const path: string = "/v1/token";
const method = "DELETE";
const get: ValidatorGetParam[] = [
  {
    minLength: 1,
    name: "id",
    required: true,
    swagger: {
      description: "Id of an entity",
      example: "",
    },
    type: "string",
  },
  {
    minLength: 1,
    name: "fields",
    required: false,
    swagger: {
      description: "Fields needed from response",
      example: "",
    },
    type: "string",
    default:
      "id,index,isOtpToken,noOfInvalid,serialNo,tokenStatus,userId,mobilePhoneNo",
  },
];
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat: ResponseFormat = {
  items: {
    fields: {
      id: {
        type: "any",
      },
      index: {
        type: "any",
      },
      isOtpToken: {
        type: "any",
      },
      noOfInvalid: {
        type: "any",
      },
      serialNo: {
        type: "string",
      },
      tokenStatus: {
        type: "string",
      },
      userId: {
        type: "string",
      },
      mobilePhoneNo: {
        type: "string",
      },
    },
    type: "object",
  },
  type: "array",
};
const successResponseFormat: ResponseFormat = {
  fields: {
    data: responseFormat,
  },
  type: "object",
};
const func = async (ctx: IRouterContext) => {
  const di: IDI = ctx.state.di;
  const query = di.validator.processQuery<{ fields: string; id: string }>(
    get,
    ctx.query
  );
  const fieldsToUse = di.partialResponsify.parseFields(
    query.fields,
    responseFormat
  );
  const theMap: Map<string, string> = new Map([
    ["id", "id"],
    ["index", "index"],
    ["isOtpToken", "isOtpToken"],
    ["noOfInvalid", "noOfInvalid"],
    ["serialNo", "serialNo"],
    ["tokenStatus", "tokenStatus"],
    ["userId", "userId"],
  ]);
  const tokenService = new TokenService(di);
  const deleteToken = await tokenService.delete(query.id);
  const result = di.partialResponsify.parseResult(
    fieldsToUse,
    responseFormat,
    di.helper.restructureArrObject([deleteToken], theMap)[0]
  );
  ctx.status = 200;
  ctx.body = { data: result };
};
export const openapiDeleteToken: IOpenApiRoute = {
  get,
  method,
  path,
  successResponseFormat,
};
export const deletedToken: IAllRoute = {
  func,
  method,
  path,
};
