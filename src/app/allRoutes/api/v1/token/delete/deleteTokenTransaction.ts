import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../../interface";
import { ValidatorGetParam } from "../../../../../../lib";
// import {
//     commonHeaderParams,
//     ICommonHeaderParamsProcessed,
// } from "../../../../../scripts";
import TokenTransactionService from "../../../../../../services/token/TokenTransactionService";

const path: string = "/v1/tokentransaction";
const method = "DELETE";
const get: ValidatorGetParam[] = [
  {
    minLength: 1,
    name: "id",
    required: true,
    swagger: {
      description: "Id of an entity",
      example: "",
    },
    type: "string",
  },
  {
    minLength: 1,
    name: "fields",
    required: false,
    swagger: {
      description: "Fields needed from response",
      example: "",
    },
    type: "string",
    default:
      "id,index,expiredDate,refNo,isUsed,factor1,factor2,factor3,factor4,factor5,factor6,factor7,factor8,factor9,factor10",
  },
];
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat: ResponseFormat = {
  items: {
    fields: {
      id: {
        type: "string",
      },
      index: {
        type: "any",
      },
      expiredDate: {
        type: "any",
      },
      refNo: {
        type: "string",
      },
      isUsed: {
        type: "any",
      },
      factor1: {
        type: "string",
      },
      factor2: {
        type: "string",
      },
      factor3: {
        type: "string",
      },
      factor4: {
        type: "string",
      },
      factor5: {
        type: "string",
      },
      factor6: {
        type: "string",
      },
      factor7: {
        type: "string",
      },
      factor8: {
        type: "string",
      },
      factor9: {
        type: "string",
      },
      factor10: {
        type: "string",
      },
    },
    type: "object",
  },
  type: "array",
};
const successResponseFormat: ResponseFormat = {
  fields: {
    data: responseFormat,
  },
  type: "object",
};
const func = async (ctx: IRouterContext) => {
  const di: IDI = ctx.state.di;
  const query = di.validator.processQuery<{ fields: string; id: string }>(
    get,
    ctx.query
  );
  const fieldsToUse = di.partialResponsify.parseFields(
    query.fields,
    responseFormat
  );
  const theMap: Map<string, string> = new Map([
    ["id", "id"],
    ["index", "index"],
    ["expiredDate", "expiredDate"],
    ["refNo", "refNo"],
    ["isUsed", "isUsed"],
    ["factor1", "factor1"],
    ["factor2", "factor2"],
    ["factor3", "factor3"],
    ["factor4", "factor4"],
    ["factor5", "factor5"],
    ["factor6", "factor6"],
    ["factor7", "factor7"],
    ["factor8", "factor8"],
    ["factor9", "factor9"],
    ["factor10", "factor10"],
  ]);
  const tokenTransaction = new TokenTransactionService(di);
  const deleteTokenTransaction = await tokenTransaction.delete(query.id);
  const result = di.partialResponsify.parseResult(
    fieldsToUse,
    responseFormat,
    di.helper.restructureArrObject([deleteTokenTransaction], theMap)[0]
  );
  ctx.status = 200;
  ctx.body = { data: result };
};
export const openapiDeleteTokenTransaction: IOpenApiRoute = {
  get,
  method,
  path,
  successResponseFormat,
};
export const deletedTokenTransaction: IAllRoute = {
  func,
  method,
  path,
};
