import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../../interface";
import { ValidatorGetParam } from "../../../../../../lib";
import TokenConfigService from "../../../../../../services/token/TokenConfigService";

const path: string = "/v1/tokenconfig";
const method = "DELETE";
const get: ValidatorGetParam[] = [
  {
    minLength: 1,
    name: "id",
    required: true,
    swagger: {
      description: "Id of an entity",
      example: "",
    },
    type: "string",
  },
  {
    minLength: 1,
    name: "fields",
    required: false,
    swagger: {
      description: "Fields needed from response",
      example: "",
    },
    type: "string",
    default: "id,index,maxInvalidToken,validity,tokenLength",
  },
];
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat: ResponseFormat = {
  fields: {
    id: {
      type: "string",
    },
    index: {
      type: "string",
    },
    maxInvalidToken: {
      type: "number",
    },
    validity: {
      type: "number",
    },
    tokenLength: {
      type: "string",
    },
  },
  type: "object",
};
const successResponseFormat: ResponseFormat = {
  fields: {
    data: responseFormat,
  },
  type: "object",
};
const func = async (ctx: IRouterContext) => {
  const di: IDI = ctx.state.di;
  const query = di.validator.processQuery<{ fields: string; id: string }>(
    get,
    ctx.query
  );
  const fieldsToUse = di.partialResponsify.parseFields(
    query.fields,
    responseFormat
  );
  const theMap: Map<string, string> = new Map([
    ["id", "id"],
    ["index", "index"],
    ["maxInvalidToken", "maxInvalidToken"],
    ["validity", "validity"],
    ["tokenLength", "tokenLength"],
  ]);
  const tokenConfigService = new TokenConfigService(di);
  const deletedToken = await tokenConfigService.delete(query.id);
  const result = di.partialResponsify.parseResult(
    fieldsToUse,
    responseFormat,
    di.helper.restructureArrObject([deletedToken], theMap)[0]
  );
  ctx.status = 200;
  ctx.body = { data: result };
};
export const openapiDeleteTokenConfig: IOpenApiRoute = {
  get,
  method,
  path,
  successResponseFormat,
};
export const deletedTokenConfig: IAllRoute = {
  func,
  method,
  path,
};
