import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../../interface";
import { ValidatorGetParam } from "../../../../../../lib";
// import {
//     commonHeaderParams,
//     ICommonHeaderParamsProcessed,
// } from "../../../../../scripts";
import TokenTransactionAuthService from "../../../../../../services/token/TokenTransactionAuthService";

const path: string = "/v1/tokentransactionauth";
const method = "DELETE";
const get: ValidatorGetParam[] = [
  {
    minLength: 1,
    name: "id",
    required: true,
    swagger: {
      description: "Id of an entity",
      example: "",
    },
    type: "string",
  },
  {
    minLength: 1,
    name: "fields",
    required: false,
    // swagger: {
    //   description: "Fields needed from response",
    //   example: "",
    // },
    type: "string",
    default: "id,index,name,noOfFactor,description",
  },
];
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat: ResponseFormat = {
  fields: {
    id: {
      type: "string",
    },
    index: {
      type: "any",
    },
    name: {
      type: "string",
    },
    noOfFactor: {
      type: "any",
    },
    description: {
      type: "string",
    },
    // tokenLength: {
    //   type: "string",
    // },
  },
  type: "object",
};
const successResponseFormat: ResponseFormat = {
  fields: {
    data: responseFormat,
  },
  type: "object",
};
const func = async (ctx: IRouterContext) => {
  const di: IDI = ctx.state.di;
  const query = di.validator.processQuery<{ fields: string; id: string }>(
    get,
    ctx.query
  );
  const fieldsToUse = di.partialResponsify.parseFields(
    query.fields,
    responseFormat
  );
  const theMap: Map<string, string> = new Map([
    ["id", "id"],
    ["index", "index"],
    ["name", "name"],
    ["noOfFactor", "noOfFactor"],
    ["description", "description"],
  ]);
  const tokenTransactionAuthService = new TokenTransactionAuthService(di);
  const deleteTokenTransactionAuth = await tokenTransactionAuthService.delete(
    query.id
  );
  const result = di.partialResponsify.parseResult(
    fieldsToUse,
    responseFormat,
    di.helper.restructureArrObject([deleteTokenTransactionAuth], theMap)[0]
  );
  ctx.status = 200;
  ctx.body = { data: result };
};
export const openapiDeleteTokenTransactionAuth: IOpenApiRoute = {
  get,
  method,
  path,
  successResponseFormat,
};
export const deletedTokenTransactionAuth: IAllRoute = {
  func,
  method,
  path,
};
