import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../../interface";
import { ValidatorGetParam, BodyFormat } from "../../../../../../lib";
import TokenTransactionService from "../../../../../../services/token/TokenTransactionService";
interface IQuery {
  [key: string]: string;
}

const path: string = "/v1/tokentransaction";
const method = "PATCH";
const get: ValidatorGetParam[] = [
  {
    minLength: 1,
    name: "fields",
    required: false,
    swagger: {
      description: "Fields needed from response",
      example: "",
    },
    type: "string",
    default:
      "id,index,expiredDate,refNo,isUsed,factor1,factor2,factor3,factor4,factor5,factor6,factor7,factor8,factor9,factor10",
  },
];
const body: BodyFormat = {
  required: true,
  type: "object",
  fields: {
    id: {
      type: "string",
      minLength: 1,
      required: true,
    },
    index: {
      type: "string",
      minLength: 1,
      required: true,
    },
    // expiredDate: {
    //   type: "string",
    //   minLength: 1,
    //   required: true,
    // },
    refNo: {
      type: "string",
      minLength: 1,
      required: true,
    },
    isUsed: {
      type: "string",
      minLength: 1,
      required: true,
    },
    factor1: {
      type: "string",
      required: true,
      minLength: 1,
    },
    factor2: {
      type: "string",
      required: true,
      minLength: 1,
    },
    factor3: {
      type: "string",
      required: true,
      minLength: 1,
    },
    factor4: {
      type: "string",
      required: true,
      minLength: 1,
    },
    factor5: {
      type: "string",
      required: true,
      minLength: 1,
    },
    factor6: {
      type: "string",
      required: true,
      minLength: 1,
    },
    factor7: {
      type: "string",
      required: true,
      minLength: 1,
    },
    factor8: {
      type: "string",
      required: true,
      minLength: 1,
    },
    factor9: {
      type: "string",
      required: true,
      minLength: 1,
    },
    factor10: {
      type: "string",
      required: true,
      minLength: 1,
    },
  },
};
const responseFormat: ResponseFormat = {
  items: {
    fields: {
      id: {
        type: "string",
      },
      index: {
        type: "any",
      },
      expiredDate: {
        type: "any",
      },
      refNo: {
        type: "string",
      },
      isUsed: {
        type: "any",
      },
      factor1: {
        type: "string",
      },
      factor2: {
        type: "string",
      },
      factor3: {
        type: "string",
      },
      factor4: {
        type: "string",
      },
      factor5: {
        type: "string",
      },
      factor6: {
        type: "string",
      },
      factor7: {
        type: "string",
      },
      factor8: {
        type: "string",
      },
      factor9: {
        type: "string",
      },
      factor10: {
        type: "string",
      },
    },
    type: "object",
  },
  type: "array",
};
const successResponseFormat: ResponseFormat = {
  fields: {
    data: responseFormat,
  },
  type: "object",
};
const func = async (ctx: IRouterContext) => {
  const di: IDI = ctx.state.di;
  console.log(ctx.request.body);
  const requestBody = di.validator.processBody<{
    id: string;
    index: number;
    // expiredDate: string,
    refNo: string;
    isUsed: number;
    factor1: string;
    factor2: string;
    factor3: string;
    factor4: string;
    factor5: string;
    factor6: string;
    factor7: string;
    factor8: string;
    factor9: string;
    factor10: string;
  }>(body, ctx.request.body);
  const query = di.validator.processQuery<{ fields: string }>(get, ctx.query);
  const fieldsToUse = di.partialResponsify.parseFields(
    query.fields,
    responseFormat
  );
  const theMap: Map<string, string> = new Map([
    ["id", "id"],
    ["index", "index"],
    ["expiredDate", "expiredDate"],
    ["refNo", "refNo"],
    ["isUsed", "isUsed"],
    ["factor1", "factor1"],
    ["factor2", "factor2"],
    ["factor3", "factor3"],
    ["factor4", "factor4"],
    ["factor5", "factor5"],
    ["factor6", "factor6"],
    ["factor7", "factor7"],
    ["factor8", "factor8"],
    ["factor9", "factor9"],
    ["factor10", "factor10"],
  ]);
  const tokenTransactionService = new TokenTransactionService(di);
  const updatedTokenTransactionService = await tokenTransactionService.update(
    requestBody
  );
  const result = di.partialResponsify.parseResult(
    fieldsToUse,
    responseFormat,
    di.helper.restructureArrObject([updatedTokenTransactionService], theMap)[0]
  );
  ctx.status = 200;
  ctx.body = { data: result };
};
export const openapiPatchTokenTransaction: IOpenApiRoute = {
  get,
  method,
  path,
  successResponseFormat,
};
export const patchTokenTransaction: IAllRoute = {
  func,
  method,
  path,
};
