import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../../interface";
import { ValidatorGetParam, BodyFormat } from "../../../../../../lib";
import TokenTransactionConfigService from "../../../../../../services/token/TokenTransactionConfigService";
interface IQuery {
  [key: string]: string;
}

const path: string = "/v1/tokentransactionconfig";
const method = "PATCH";
const get: ValidatorGetParam[] = [
  {
    minLength: 1,
    name: "fields",
    required: false,
    swagger: {
      description: "Fields needed from response",
      example: "",
    },
    type: "string",
    default: "id,index,name,trxCode",
  },
];
const body: BodyFormat = {
  required: true,
  type: "object",
  fields: {
    id: {
      type: "string",
      minLength: 1,
      required: true,
    },
    index: {
      type: "string",
      minLength: 1,
      required: true,
    },
    name: {
      type: "string",
      minLength: 1,
      required: true,
    },
    trxCode: {
      type: "string",
      minLength: 1,
      required: true,
    },
  },
};
const responseFormat: ResponseFormat = {
  fields: {
    id: {
      type: "string",
    },
    index: {
      type: "any",
    },
    name: {
      type: "string",
    },
    trxCode: {
      type: "string",
    },
  },
  type: "object",
};
const successResponseFormat: ResponseFormat = {
  fields: {
    data: responseFormat,
  },
  type: "object",
};
const func = async (ctx: IRouterContext) => {
  const di: IDI = ctx.state.di;
  console.log(ctx.request.body);
  const requestBody = di.validator.processBody<{
    id: string;
    name: string;
    index: number;
    trxCode: string;
  }>(body, ctx.request.body);
  const query = di.validator.processQuery<{ fields: string }>(get, ctx.query);
  const fieldsToUse = di.partialResponsify.parseFields(
    query.fields,
    responseFormat
  );
  const theMap: Map<string, string> = new Map([
    ["id", "id"],
    ["index", "index"],
    ["name", "name"],
    ["trxCode", "trxCode"],
  ]);
  const tokenTransactionAuthService = new TokenTransactionConfigService(di);
  const updatedTokenTransactionAuth = await tokenTransactionAuthService.update(
    requestBody
  );
  const result = di.partialResponsify.parseResult(
    fieldsToUse,
    responseFormat,
    di.helper.restructureArrObject([updatedTokenTransactionAuth], theMap)[0]
  );
  ctx.status = 200;
  ctx.body = { data: result };
};
export const openapiPatchTokenTransactionConfig: IOpenApiRoute = {
  get,
  method,
  path,
  successResponseFormat,
};
export const patchTokenTransactionConfig: IAllRoute = {
  func,
  method,
  path,
};
