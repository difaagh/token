import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../../interface";
import {
  ValidatorGetParam,
  ValidatorHeaderParam,
  BodyFormat,
} from "../../../../../../lib";
// import {
//   commonHeaderParams,
//   ICommonHeaderParamsProcessed,
// } from "../../../../../scripts";
import TokenConfigService from "../../../../../../services/token/TokenConfigService";
interface IQuery {
  [key: string]: string;
}

const path: string = "/v1/tokenconfig";
const method = "PUT";
const get: ValidatorGetParam[] = [
  {
    minLength: 1,
    name: "fields",
    required: false,
    swagger: {
      description: "Fields needed from response",
      example: "",
    },
    type: "string",
    default: "id,index,maxInvalidToken,validity,tokenLength",
  },
];
const body: BodyFormat = {
  required: true,
  type: "object",
  fields: {
    id: {
      type: "string",
      minLength: 1,
      required: true,
    },
    index: {
      type: "string",
      minLength: 1,
      required: true,
    },
    maxInvalidToken: {
      type: "string",
      minLength: 1,
      required: true,
    },
    validity: {
      type: "string",
      minLength: 1,
      required: true,
    },
    tokenLength: {
      type: "string",
      minLength: 1,
      required: true,
    },
  },
};
const responseFormat: ResponseFormat = {
  fields: {
    id: {
      type: "string",
    },
    index: {
      type: "string",
    },
    maxInvalidToken: {
      type: "string",
    },
    validity: {
      type: "string",
    },
    tokenLength: {
      type: "string",
    },
  },
  type: "object",
};
const successResponseFormat: ResponseFormat = {
  fields: {
    data: responseFormat,
  },
  type: "object",
};
const func = async (ctx: IRouterContext) => {
  const di: IDI = ctx.state.di;
  console.log(ctx.request.body);
  const requestBody = di.validator.processBody<{
    id: string;
    index: string;
    maxInvalidToken: number;
    validity: number;
    tokenLength: string;
  }>(body, ctx.request.body);
  const query = di.validator.processQuery<{ fields: string }>(get, ctx.query);
  const fieldsToUse = di.partialResponsify.parseFields(
    query.fields,
    responseFormat
  );
  const theMap: Map<string, string> = new Map([
    ["id", "id"],
    ["index", "index"],
    ["maxInvalidToken", "maxInvalidToken"],
    ["validity", "validity"],
    ["tokenLength", "tokenLength"],
  ]);
  const tokenConfigService = new TokenConfigService(di);
  const result = di.partialResponsify.parseResult(
    fieldsToUse,
    responseFormat,
    di.helper.restructureArrObject(
      [await tokenConfigService.create(requestBody)],
      theMap
    )[0]
  );
  ctx.status = 200;
  ctx.body = { data: result };
};
export const openapiPutTokenConfig: IOpenApiRoute = {
  get,
  method,
  path,
  successResponseFormat,
};
export const putTokenConfig: IAllRoute = {
  func,
  method,
  path,
};
