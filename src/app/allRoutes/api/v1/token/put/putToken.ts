import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../../interface";
import {
  ValidatorGetParam,
  ValidatorHeaderParam,
  BodyFormat,
} from "../../../../../../lib";
// import {
//   commonHeaderParams,
//   ICommonHeaderParamsProcessed,
// } from "../../../../../scripts";
import TokenService from "../../../../../../services/token/TokenService";
interface IQuery {
  [key: string]: string;
}

const path: string = "/v1/token";
const method = "PUT";
const get: ValidatorGetParam[] = [
  {
    minLength: 1,
    name: "fields",
    required: false,
    swagger: {
      description: "Fields needed from response",
      example: "",
    },
    type: "string",
    default:
      "id,index,isOtpToken,noOfInvalid,serialNo,tokenStatus,userId,mobilePhoneNo",
  },
];
const body: BodyFormat = {
  required: true,
  type: "object",
  fields: {
    id: {
      type: "string",
      minLength: 1,
      required: true,
    },
    index: {
      type: "string",
      minLength: 1,
      required: true,
    },
    isOtpToken: {
      type: "string",
      minLength: 1,
      required: true,
    },
    noOfInvalid: {
      type: "string",
      minLength: 1,
      required: true,
    },
    serialNo: {
      type: "string",
      minLength: 1,
      required: true,
    },
    tokenStatus: {
      type: "string",
      minLength: 1,
      required: true,
    },
    userId: {
      type: "string",
      minLength: 1,
      required: true,
    },
    mobilePhoneNo: {
      type: "string",
      minLength: 1,
      required: true,
    },
  },
};
const responseFormat: ResponseFormat = {
  items: {
    fields: {
      id: {
        type: "any",
      },
      index: {
        type: "any",
      },
      isOtpToken: {
        type: "any",
      },
      noOfInvalid: {
        type: "any",
      },
      serialNo: {
        type: "string",
      },
      tokenStatus: {
        type: "string",
      },
      userId: {
        type: "string",
      },
      mobilePhoneNo: {
        type: "string",
      },
    },
    type: "object",
  },
  type: "array",
};
const successResponseFormat: ResponseFormat = {
  fields: {
    data: responseFormat,
  },
  type: "object",
};
const func = async (ctx: IRouterContext) => {
  const di: IDI = ctx.state.di;
  console.log(ctx.request.body);
  const requestBody = di.validator.processBody<{
    id: string;
    index: number;
    isOtpToken: number;
    noOfInvalid: number;
    serialNo: string;
    tokenStatus: string;
    userId: string;
    mobilePhoneNo: string;
  }>(body, ctx.request.body);
  const query = di.validator.processQuery<{ fields: string }>(get, ctx.query);
  const fieldsToUse = di.partialResponsify.parseFields(
    query.fields,
    responseFormat
  );
  const theMap: Map<string, string> = new Map([
    ["id", "id"],
    ["index", "index"],
    ["isOtpToken", "isOtpToken"],
    ["noOfInvalid", "noOfInvalid"],
    ["serialNo", "serialNo"],
    ["tokenStatus", "tokenStatus"],
    ["userId", "userId"],
  ]);
  const tokenService = new TokenService(di);
  const result = di.partialResponsify.parseResult(
    fieldsToUse,
    responseFormat,
    di.helper.restructureArrObject(
      [await tokenService.update(requestBody)],
      theMap
    )[0]
  );
  ctx.status = 200;
  ctx.body = { data: result };
};
export const openapiPutToken: IOpenApiRoute = {
  get,
  method,
  path,
  successResponseFormat,
};
export const putToken: IAllRoute = {
  func,
  method,
  path,
};
