import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../../interface";
import {
  ValidatorGetParam,
  ValidatorHeaderParam,
  BodyFormat,
} from "../../../../../../lib";
// import {
//   commonHeaderParams,
//   ICommonHeaderParamsProcessed,
// } from "../../../../../scripts";
import TokenTransactionAuthService from "../../../../../../services/token/TokenTransactionAuthService";
interface IQuery {
  [key: string]: string;
}

const path: string = "/v1/tokentransactionauth";
const method = "PUT";
const get: ValidatorGetParam[] = [
  {
    minLength: 1,
    name: "fields",
    required: false,
    swagger: {
      description: "Fields needed from response",
      example: "",
    },
    type: "string",
    default: "id,index,name,noOfFactor,description",
  },
];
const body: BodyFormat = {
  required: true,
  type: "object",
  fields: {
    id: {
      type: "string",
      minLength: 1,
      required: true,
    },
    index: {
      type: "string",
      minLength: 1,
      required: true,
    },
    name: {
      type: "string",
      minLength: 1,
      required: true,
    },
    noOfFactor: {
      type: "string",
      minLength: 1,
      required: true,
    },
    description: {
      type: "string",
      minLength: 1,
      required: true,
    },
  },
};
const responseFormat: ResponseFormat = {
  fields: {
    id: {
      type: "string",
    },
    index: {
      type: "any",
    },
    name: {
      type: "string",
    },
    noOfFactor: {
      type: "any",
    },
    description: {
      type: "string",
    },
  },
  type: "object",
};
const successResponseFormat: ResponseFormat = {
  fields: {
    data: responseFormat,
  },
  type: "object",
};
const func = async (ctx: IRouterContext) => {
  const di: IDI = ctx.state.di;
  console.log(ctx.request.body);
  const requestBody = di.validator.processBody<{
    id: string;
    name: string;
    index: number;
    noOfFactor: string;
    description: string;
  }>(body, ctx.request.body);
  const query = di.validator.processQuery<{ fields: string }>(get, ctx.query);
  const fieldsToUse = di.partialResponsify.parseFields(
    query.fields,
    responseFormat
  );
  const theMap: Map<string, string> = new Map([
    ["id", "id"],
    ["index", "index"],
    ["name", "name"],
    ["noOfFactor", "noOfFactor"],
    ["description", "description"],
  ]);
  const tokenTransactionAuthService = new TokenTransactionAuthService(di);
  const result = di.partialResponsify.parseResult(
    fieldsToUse,
    responseFormat,
    di.helper.restructureArrObject(
      [await tokenTransactionAuthService.create(requestBody)],
      theMap
    )[0]
  );
  ctx.status = 200;
  ctx.body = { data: result };
};
export const openapiPutTokenTransactionAuth: IOpenApiRoute = {
  get,
  method,
  path,
  successResponseFormat,
};
export const putTokenTransactionAuth: IAllRoute = {
  func,
  method,
  path,
};
