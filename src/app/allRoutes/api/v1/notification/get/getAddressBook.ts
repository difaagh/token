import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../../interface";
// import { ValidatorGetParam, ValidatorHeaderParam } from "../../../../../lib";
// import {
//     commonHeaderParams,
//     ICommonHeaderParamsProcessed,
// } from "../../../../../scripts";
import AddressbookService from "../../../../../../services/notification/AddressbookService";
import {
  pageableGetParamValidator,
  PageableGetParam,
  ValidatorGetParam,
} from "../../../../../../lib";
const path: string = "/v1/addressbook";
const method = "GET";
const get: ValidatorGetParam[] = [
  ...pageableGetParamValidator,
  {
    minLength: 1,
    name: "fields",
    required: false,
    swagger: {
      description: "Fields needed from response",
      example: "",
    },
    type: "string",
    default:
      "id,address,channel,deviceId,deviceType,userId,createdBy,dateCreated",
  },
];
const responseFormat: ResponseFormat = {
  items: {
    fields: {
      id: {
        type: "any",
      },
      address: {
        type: "any",
      },
      channel: {
        type: "any",
      },
      deviceId: {
        type: "any",
      },
      deviceType: {
        type: "any",
      },
      userId: {
        type: "any",
      },
      createdBy: {
        type: "any",
      },
      dateCreated: {
        type: "any",
      },
    },
    type: "object",
  },
  type: "array",
};
const successResponseFormat: ResponseFormat = {
  fields: {
    data: responseFormat,
  },
  type: "object",
};
const func = async (ctx: IRouterContext) => {
  const di: IDI = ctx.state.di;
  const query = di.validator.processQuery<
    PageableGetParam & { fields: string }
  >(get, ctx.query);
  const fieldsToUse = di.partialResponsify.parseFields(
    query.fields,
    responseFormat
  );
  const tokenService = new AddressbookService(di);
  const theMap: Map<string, string> = new Map([
    ["id", "id"],
    ["address", "address"],
    ["channel", "channel"],
    ["deviceId", "deviceId"],
    ["deviceType", "deviceType"],
    ["userId", "userId"],
    ["createdBy", "createdBy"],
    ["dateCreated", "dateCreated"],
  ]);
  const result = di.partialResponsify.parseResult(
    fieldsToUse,
    responseFormat,
    di.helper.restructureArrObject(await tokenService.findAll(query), theMap)
  );
  ctx.status = 200;
  ctx.body = { data: result };
};
export const openapiGetAddressBook: IOpenApiRoute = {
  get,
  method,
  path,
  successResponseFormat,
  // tags: ["masterdata"],
};
export const getAddressBook: IAllRoute = {
  func,
  method,
  path,
};
