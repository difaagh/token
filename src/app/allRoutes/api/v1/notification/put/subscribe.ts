import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../../interface";
import {
  ValidatorGetParam,
  ValidatorHeaderParam,
  BodyFormat,
} from "../../../../../../lib";
import NotificationService from "../../../../../../services/notification/NotificationService";
interface IQuery {
  [key: string]: string;
}

const path: string = "/v1/notification/subscribe";
const method = "PUT";
const get: ValidatorGetParam[] = [
  {
    minLength: 1,
    name: "fields",
    required: false,
    swagger: {
      description: "Fields needed from response",
      example: "",
    },
    type: "string",
    default: "userId,channel,deviceType,deviceId",
  },
];
const body: BodyFormat = {
  required: true,
  type: "object",
  fields: {
    userId: {
      type: "string",
      minLength: 1,
      required: true,
    },
    channel: {
      type: "string",
      required: false,
    },
    deviceType: {
      type: "string",
      minLength: 1,
      required: true,
    },
    deviceId: {
      type: "string",
      minLength: 1,
      required: true,
    },
  },
};
const responseFormat: ResponseFormat = {
  fields: {
    userId: {
      type: "any",
    },
    channel: {
      type: "any",
    },
    deviceType: {
      type: "any",
    },
    deviceId: {
      type: "any",
    },
  },
  type: "object",
};
const successResponseFormat: ResponseFormat = {
  fields: {
    data: responseFormat,
  },
  type: "object",
};
const func = async (ctx: IRouterContext) => {
  const di: IDI = ctx.state.di;
  const requestBody = di.validator.processBody<{
    userId: string;
    channel: string;
    deviceType: string;
    deviceId: string;
  }>(body, ctx.request.body);
  const query = di.validator.processQuery<{ fields: string }>(get, ctx.query);
  const fieldsToUse = di.partialResponsify.parseFields(
    query.fields,
    responseFormat
  );
  const theMap: Map<string, string> = new Map([
    ["userId", "userId"],
    ["channel", "channel"],
    ["deviceId", "deviceId"],
    ["deviceType", "deviceType"],
  ]);
  const notificationService = new NotificationService(di);
  // const result = di.partialResponsify.parseResult(
  //   fieldsToUse,
  //   responseFormat,
  //   di.helper.restructureArrObject(
  //     [await notificationService.subscribe(requestBody)],
  //     theMap
  //   )[0]
  // );
  await notificationService.subscribe(requestBody).catch((err) => {
    if (err) throw new Error(`error ${err}`);
  });
  ctx.status = 200;
  ctx.body = { data: `success subscribe to ${requestBody.userId}` };
};
export const openapiSubscribe: IOpenApiRoute = {
  get,
  method,
  path,
  successResponseFormat,
};
export const subscribe: IAllRoute = {
  func,
  method,
  path,
};
