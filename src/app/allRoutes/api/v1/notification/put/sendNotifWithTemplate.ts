import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../../interface";
import {
  ValidatorGetParam,
  ValidatorHeaderParam,
  BodyFormat,
} from "../../../../../../lib";
import NotificationService from "../../../../../../services/notification/NotificationService";
interface IQuery {
  [key: string]: string;
}

const path: string = "/v1/notificationtemplate";
const method = "PUT";
const get: ValidatorGetParam[] = [
  {
    minLength: 1,
    name: "fields",
    required: false,
    swagger: {
      description: "Fields needed from response",
      example: "",
    },
    type: "string",
    default: "userId,channel,subject,message,templateCode,valueMap",
  },
];
const body: BodyFormat = {
  required: true,
  type: "object",
  fields: {
    userId: {
      type: "string",
      minLength: 1,
      required: true,
    },
    channel: {
      type: "string",
      minLength: 1,
      required: false,
    },
    subject: {
      type: "string",
      minLength: 1,
      required: true,
    },
    message: {
      type: "string",
      minLength: 1,
      required: true,
    },
    templateCode: {
      type: "string",
      minLength: 1,
      required: true,
    },
    valueMap: {
      items: {
        type: "string",
        required: true,
      },
      type: "array",
      required: true,
    },
  },
};
const responseFormat: ResponseFormat = {
  fields: {
    userId: {
      type: "any",
    },
    channel: {
      type: "any",
    },
    subject: {
      type: "any",
    },
    message: {
      type: "any",
    },
    templateCode: {
      type: "any",
    },
    valueMap: {
      type: "any",
    },
  },
  type: "object",
};
const successResponseFormat: ResponseFormat = {
  fields: {
    data: responseFormat,
  },
  type: "object",
};
const func = async (ctx: IRouterContext) => {
  const di: IDI = ctx.state.di;
  console.log(ctx.request.body);
  const requestBody = di.validator.processBody<{
    userId: string;
    channel: string;
    subject: string;
    message: string;
    templateCode: string;
    valueMap: object;
  }>(body, ctx.request.body);
  const query = di.validator.processQuery<{ fields: string }>(get, ctx.query);
  const fieldsToUse = di.partialResponsify.parseFields(
    query.fields,
    responseFormat
  );
  const theMap: Map<string, string> = new Map([
    ["userId", "userId"],
    ["channel", "channel"],
    ["subject", "subject"],
    ["message", "message"],
    ["templateCode", "templateCode"],
    ["valueMap", "valueMap"],
  ]);
  const notificationService = new NotificationService(di);
  // const result = di.partialResponsify.parseResult(
  //   fieldsToUse,
  //   responseFormat,
  //   di.helper.restructureArrObject(
  //     [await notificationService.send(requestBody)],
  //     theMap
  //   )[0]
  // );
  await notificationService.sendWithTemplate(requestBody).catch((err) => {
    if (err) throw new Error(`error ${err}`);
  });
  ctx.status = 200;
  ctx.body = { data: `success send notification to ${requestBody.userId}` };
};
export const openapiSendNotifWithTemplate: IOpenApiRoute = {
  get,
  method,
  path,
  successResponseFormat,
};
export const sendNotifWithTemplate: IAllRoute = {
  func,
  method,
  path,
};
