import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../../interface";
import { ValidatorGetParam, BodyFormat } from "../../../../../../lib";
import NotificationService from "../../../../../../services/notification/NotificationService";

const path: string = "/v1/notification/unsubscribe";
const method = "PUT";
const get: ValidatorGetParam[] = [
  {
    minLength: 1,
    name: "fields",
    required: false,
    swagger: {
      description: "Fields needed from response",
      example: "",
    },
    type: "string",
    default: "userId",
  },
];
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat: ResponseFormat = {
  items: {
    fields: {
      userId: {
        type: "any",
      },
    },
    type: "object",
  },
  type: "array",
};
const body: BodyFormat = {
  required: true,
  type: "object",
  fields: {
    userId: {
      type: "string",
      required: true,
    },
  },
};
const successResponseFormat: ResponseFormat = {
  fields: {
    data: responseFormat,
  },
  type: "object",
};
const func = async (ctx: IRouterContext) => {
  const di: IDI = ctx.state.di;
  const requestBody = di.validator.processBody<{
    userId: string;
  }>(body, ctx.request.body);
  const query = di.validator.processQuery<{ fields: string }>(get, ctx.query);
  const fieldsToUse = di.partialResponsify.parseFields(
    query.fields,
    responseFormat
  );
  const theMap: Map<string, string> = new Map([["userId", "userId"]]);
  const notificationService = new NotificationService(di);
  await notificationService.unsubscribe(requestBody.userId).catch((err) => {
    if (err) throw new Error(`error ${err}`);
  });
  ctx.status = 200;
  ctx.body = { data: "success to unsubscribe" };
};
export const openapiUnsubscribe: IOpenApiRoute = {
  get,
  method,
  path,
  successResponseFormat,
};
export const unsubscribe: IAllRoute = {
  func,
  method,
  path,
};
