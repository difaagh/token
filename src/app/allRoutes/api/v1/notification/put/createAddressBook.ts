import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../../interface";
import {
  ValidatorGetParam,
  ValidatorHeaderParam,
  BodyFormat,
} from "../../../../../../lib";
import AddressBookService from "../../../../../../services/notification/AddressBookService";
interface IQuery {
  [key: string]: string;
}

const path: string = "/v1/addressbook";
const method = "PUT";
const get: ValidatorGetParam[] = [
  {
    minLength: 1,
    name: "fields",
    required: false,
    swagger: {
      description: "Fields needed from response",
      example: "",
    },
    type: "string",
    default: "id,address,channel,deviceId,deviceType,userId",
  },
];
const body: BodyFormat = {
  required: true,
  type: "object",
  fields: {
    id: {
      type: "string",
      minLength: 1,
      required: true,
    },
    address: {
      type: "string",
      minLength: 1,
      required: true,
    },
    channel: {
      type: "string",
      minLength: 1,
      required: true,
    },
    deviceId: {
      type: "string",
      minLength: 1,
      required: true,
    },
    deviceType: {
      type: "string",
      minLength: 1,
      required: true,
    },
    userId: {
      type: "string",
      minLength: 1,
      required: true,
    },
  },
};
const responseFormat: ResponseFormat = {
  fields: {
    id: {
      type: "any",
    },
    address: {
      type: "any",
    },
    channel: {
      type: "any",
    },
    deviceId: {
      type: "any",
    },
    deviceType: {
      type: "any",
    },
    userId: {
      type: "any",
    },
  },
  type: "object",
};
const successResponseFormat: ResponseFormat = {
  fields: {
    data: responseFormat,
  },
  type: "object",
};
const func = async (ctx: IRouterContext) => {
  const di: IDI = ctx.state.di;
  console.log(ctx.request.body);
  const requestBody = di.validator.processBody<{
    id: string;
    address: string;
    channel: string;
    deviceId: string;
    deviceType: string;
    userId: string;
  }>(body, ctx.request.body);
  const query = di.validator.processQuery<{ fields: string }>(get, ctx.query);
  const fieldsToUse = di.partialResponsify.parseFields(
    query.fields,
    responseFormat
  );
  const theMap: Map<string, string> = new Map([
    ["id", "id"],
    ["address", "address"],
    ["channel", "channel"],
    ["deviceId", "deviceId"],
    ["deviceType", "deviceType"],
    ["userId", "userId"],
  ]);
  const addressBookService = new AddressBookService(di);
  // const result = di.partialResponsify.parseResult(
  //   fieldsToUse,
  //   responseFormat,
  //   di.helper.restructureArrObject(
  //     [await tokenService.create(requestBody)],
  //     theMap
  //   )[0]
  // );
  const process = await addressBookService
    .create(requestBody)
    .catch((err: any) => {
      if (err) {
        console.log(err);
        throw new Error(`error ${err}`);
      }
    });
  ctx.status = 200;
  ctx.body = { data: "success to create" };
};
export const openapiCreateAddressBook: IOpenApiRoute = {
  get,
  method,
  path,
  successResponseFormat,
};
export const createAddressBook: IAllRoute = {
  func,
  method,
  path,
};
