import { IRouterContext } from "koa-router";
import { IAllRoute,  IDI, IOpenApiRoute } from "../../../../../interface";
const uptime = async (ctx: IRouterContext) => {
    ctx.body = {
        uptime: process.uptime(),
    };
};
export const openapiGetUptime: IOpenApiRoute = {
    method: "GET",
    path: "/v1/",
    successResponseFormat: {
        fields: {
            uptime: {
                type: "number",
            },
        },
        type: "object",
    },
};
export const getUptime: IAllRoute = {
    func: uptime,
    method: "GET",
    path: "/v1/",
};

