import { IAllRoute, IOpenApiRoute } from "../../../../interface";
import { getUptime, openapiGetUptime } from "./get/uptime";

// notification
import {
  getAddressBook,
  openapiGetAddressBook,
} from "./notification/get/getAddressBook";
import {
  createAddressBook,
  openapiCreateAddressBook,
} from "./notification/put/createAddressBook";
import {
  sendNotification,
  openapiSendNotification,
} from "./notification/put/sendNotification";
import {
  sendNotifWithTemplate,
  openapiSendNotifWithTemplate,
} from "./notification/put/sendNotifWithTemplate";
import { subscribe, openapiSubscribe } from "./notification/put/subscribe";
import { unsubscribe, openapiUnsubscribe } from "./notification/put/unsubscibe";

// template
import { getTemplate, openapiGetTemplate } from "./template/get/getTemplate";
import { putTemplate, openapiPutTemplate } from "./template/put/putTemplate";
import {
  patchTemplate,
  openapiPatchTemplate,
} from "./template/patch/patchTemplate";
import {
  deletedTemplate,
  openapiDeleteTemplate,
} from "./template/delete/deleteTemplate";

// token otp
import {
  getTokenConfig,
  openapiGetTokenConfig,
} from "./token/get/getTokenConfig";
import {
  getTokenTransactionConfig,
  openapiGetTokenTransactionConfig,
} from "./token/get/getTokenTransactionConfig";
import {
  getTokenTransactionAuth,
  openapiGetTokenTransactionAuth,
} from "./token/get/getTokenTransactionAuth";
import {
  getTokenTransaction,
  openapiGetTokenTransaction,
} from "./token/get/getTokenTransaction";
import { getToken, openapiToken } from "./token/get/getToken";

// delete
import {
  deletedTokenConfig,
  openapiDeleteTokenConfig,
} from "./token/delete/deleteTokenConfig";
import { deletedToken, openapiDeleteToken } from "./token/delete/deleteToken";
import {
  deletedTokenTransaction,
  openapiDeleteTokenTransaction,
} from "./token/delete/deleteTokenTransaction";
import {
  deletedTokenTransactionAuth,
  openapiDeleteTokenTransactionAuth,
} from "./token/delete/deleteTokenTransactionAuth";

// put

import {
  putTokenConfig,
  openapiPutTokenConfig,
} from "./token/put/putTokenConfig";
import {
  putTokenTransactionAuth,
  openapiPutTokenTransactionAuth,
} from "./token/put/putTokenTransactionAuth";
import {
  putTokenTransaction,
  openapiPutTokenTransaction,
} from "./token/put/putTokenTransaction";
import {
  putTokenTransactionConfig,
  openapiPutTokenTransactionConfig,
} from "./token/put/putTokenTransactionConfig";
import { putToken, openapiPutToken } from "./token/put/putToken";

// patch
import {
  patchTokenConfig,
  openapiPatchTokenConfig,
} from "./token/patch/patchTokenConfig";
import { patchToken, openapiPatchToken } from "./token/patch/patchToken";
import {
  patchTokenTransaction,
  openapiPatchTokenTransaction,
} from "./token/patch/patchTokenTransaction";
import {
  patchTokenTransactionAuth,
  openapiPatchTokenTransactionAuth,
} from "./token/patch/patchTokenTransactionAuth";
import {
  patchTokenTransactionConfig,
  openapiPatchTokenTransactionConfig,
} from "./token/patch/patchTokenTransactionConfig";

import {
  openapiGetTemplateChan,
  getTemplateChan,
} from "./template/get/getTemplateChannel";

interface IRouteTuple {
  0: IOpenApiRoute;
  1: IAllRoute;
}

const routes: IRouteTuple[] = [
  [openapiGetUptime, getUptime],
  [openapiGetTokenConfig, getTokenConfig],
  [openapiDeleteTokenConfig, deletedTokenConfig],
  [openapiPatchTokenConfig, patchTokenConfig],
  [openapiPutTokenConfig, putTokenConfig],
  [openapiGetTokenTransactionConfig, getTokenTransactionConfig],
  [openapiToken, getToken],
  [openapiGetTokenTransactionAuth, getTokenTransactionAuth],
  [openapiGetTokenTransaction, getTokenTransaction],
  [openapiDeleteToken, deletedToken],
  [openapiPutToken, putToken],
  [openapiPatchToken, patchToken],
  [openapiPutTokenTransaction, putTokenTransaction],
  [openapiPatchTokenTransaction, patchTokenTransaction],
  [openapiDeleteTokenTransaction, deletedTokenTransaction],
  [openapiPatchTokenTransactionAuth, patchTokenTransactionAuth],
  [openapiPutTokenTransactionAuth, putTokenTransactionAuth],
  [openapiDeleteTokenTransactionAuth, deletedTokenTransactionAuth],
  [openapiPutTokenTransactionConfig, putTokenTransactionConfig],
  [openapiPatchTokenTransactionConfig, patchTokenTransactionConfig],
  [openapiCreateAddressBook, createAddressBook],
  [openapiGetAddressBook, getAddressBook],
  [openapiGetTemplate, getTemplate],
  [openapiPutTemplate, putTemplate],
  [openapiPatchTemplate, patchTemplate],
  [openapiGetTemplateChan, getTemplateChan],
  [openapiDeleteTemplate, deletedTemplate],
  [openapiSendNotification, sendNotification],
  [openapiSendNotifWithTemplate, sendNotifWithTemplate],
  [openapiSubscribe, subscribe],
  [openapiUnsubscribe, unsubscribe],
];

const tmpBaseOpenApiRoutes: IOpenApiRoute[] = [];
const tmpBaseRoutes: IAllRoute[] = [];
for (const route of routes) {
  if (!route[1].path.startsWith("/v1/") || !route[0].path.startsWith("/v1/")) {
    // tslint:disable-next-line: no-console
    console.log(route[1]);
    throw new Error("path should start with v1");
  }
  tmpBaseOpenApiRoutes.push(route[0]);
  tmpBaseRoutes.push(route[1]);
}

// seperate the route to openapi and application route, future easier to extend
export const baseOpenApiRoutes: IOpenApiRoute[] = tmpBaseOpenApiRoutes;
export const baseRoutes: IAllRoute[] = tmpBaseRoutes;
