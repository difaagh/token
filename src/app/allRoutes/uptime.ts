import { IRouterContext } from "koa-router";

export const uptime = async (ctx: IRouterContext) => {
    ctx.body = {
        uptime: process.uptime(),
    };
};
