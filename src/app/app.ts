import * as Koa from "koa";
import * as cors from "koa2-cors";
import "reflect-metadata";
import { getDiComponent } from "../di";
import { Config } from "../lib";
import { di, errorCatcher } from "../middleware";
import { AppRoutes } from "./appRoutes";
import * as admin from "firebase-admin";

export async function getApp() {
  const app = new Koa();
  const config = new Config();
  admin.initializeApp({
    credential: admin.credential.cert(config.firebaseServiceAccount),
  });
  // should use config to control
  // app.use(koaHelmet());
  app.use(errorCatcher);
  app.use(di(await getDiComponent(config)));
  // app.use(commonHeader);
  app.use(cors());
  // if (config.swagger) {
  //     // maybe add a 301 reroute from swagger to swagger/ in future
  //     app.use(koaMount("/swagger/", koaStatic(__dirname + "/../../swagger-ui-dist")));
  // }
  const appRoutes = new AppRoutes(false);
  app.use(appRoutes.getRoutes());
  // it is optional though, but test got this for testing purpose
  // app.use(appRoutes.getAllowedMethods());
  return { app, config };
}
