import * as Koa from "koa";
import { PartialResponsifyValidationError } from "partial-responsify";
import { Tracer, ValidatorError } from "../lib";

enum ErrorCodes {
  E0 = "E0", // All Other Unctch Error
  EV = "EV", // ValidatorError
  EV2 = "EV2", // PartialResponsifyValidationError
  EDOPAR = "EDOPAR", // DOPA Still Running
  EDOPAF = "EDOPAF", // DOPA Failed
  EIK = "EIK", // InvalidKeyError
  ENF = "ENF", // DataNotFoundError
  EID = "EID", // InsufficientDataError
  ENL = "ENL", // NoUnitholderIdError
  ENL2 = "ENL2", // DocumentNotUploadedError
  ENL3 = "ENL3", // DocumentNotAllowUpdateError
  ENL4 = "ENL4", // CustomerStatusNotAllowUpdateError
  ENL5 = "ENL5", // NotApprovedCustomerError
  EPV = "EPV", // PentacleValidationError
  EPMV = "EPMV", // PentacleMultipleValidationError
  ECPAMC = "ECPAMC", // AmlaCheckFailError
  ECPAMV = "ECPAMV", // AmlaCheckFailError
  EPA = "EPA", // AmlaCheckFailError
  EPC = "EPC", // PentacleCriticalError
  EDR = "EDR", // DateRangeError
  ENA = "ENA", // NotAuthorizeError
  EMA = "EMA", // MiroserviceApiError
  EDE = "EDE", // DuplicateEntryError
}
export async function errorCatcher(ctx: Koa.Context, next: () => Promise<any>) {
  try {
    await next();
  } catch (err) {
    // future use contentLanguage to turn ValidatorError and PartialResponsifyValidationError to multilanguage;
    const tracer: Tracer = ctx.state.tracer || null;
    // const contentLanguage = ctx.state.contentLanguage || "en";
    let status = err.status || 500;
    const errorCode = ErrorCodes.E0;
    const body: {
      _tracer?: any;
      errorCode: string;
      errorDetails?: Array<{
        errorCode: string;
        errorDesc: string;
      }>;
      message: string;
    } = {
      errorCode,
      message: err.message,
    };
    if (err instanceof ValidatorError) {
      status = 400;
      body.errorCode = ErrorCodes.EV;
    } else if (err instanceof PartialResponsifyValidationError) {
      status = 400;
      body.errorCode = ErrorCodes.EV2;
      console.log(err.formatErrs && err.formatErrs.map((field) => field.name));
    }
    console.log(err);
    ctx.status = status;
    if (tracer && tracer.trace) {
      body._tracer = tracer.format();
    }
    ctx.body = body;
    if (status === 500) {
      ctx.app.emit("error", err, ctx);
    }
  }
}
