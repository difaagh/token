import { BaseEntity, Column } from "typeorm";

export default class Maintenable extends BaseEntity {
  @Column({ name: "created_by" })
  public createdBy: string;
  @Column({ name: "date_created" })
  public dateCreated: Date;
}
