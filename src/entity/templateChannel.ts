import {
  BaseEntity,
  Column,
  PrimaryColumn,
  Entity,
  OneToOne,
  JoinColumn,
  ManyToOne,
  JoinTable,
  PrimaryGeneratedColumn,
  BeforeInsert,
  Generated,
} from "typeorm";
import { TemplateEntity } from "./template";
import Updatedable from "./updatedable";

@Entity({ name: "not_template_channel" })
export class TemplateChannelEntity extends Updatedable {
  @PrimaryGeneratedColumn({ name: "id" })
  @Generated("uuid")
  public id: string;
  @Column({ name: "version" })
  public version: number;
  @Column({ name: "channel" })
  public channel: string;
  @Column({ name: "content" })
  public content: string;
  @Column({ name: "inactive_flag" })
  public innactiveFlag: boolean;
  @Column({ name: "subject" })
  public subject: string;
  @ManyToOne(() => TemplateEntity, (templateEntity) => templateEntity.code)
  @JoinColumn({ name: "template_cd" })
  // @JoinColumn({ name: "code" })
  public templates: TemplateEntity;
  @Column({ name: "template_cd" })
  public templateCd: string;
}
