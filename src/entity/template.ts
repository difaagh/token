import {
  Entity,
  OneToMany,
  Column,
  PrimaryColumn,
  JoinColumn,
  OneToOne,
  RelationId,
} from "typeorm";
import { TemplateChannelEntity } from "./templateChannel";
import Updatedable from "./updatedable";
import { TemplateParameterEntity } from "./templateParameter";

@Entity({ name: "not_template" })
export class TemplateEntity extends Updatedable {
  @OneToMany(() => TemplateChannelEntity, (e) => e.templates)
  public templateChannel: any[];
  @OneToMany(() => TemplateParameterEntity, (e) => e.template)
  public parameter: Map<string, TemplateParameterEntity["identifier"]>;
  @PrimaryColumn({ name: "code" })
  public code: string;
  @Column({ name: "version" })
  public version: number;
  @Column({ name: "delete_flag" })
  public deleteFlag: boolean;
  @Column({ name: "description" })
  public description: string;
  @Column({ name: "inactive_flag" })
  public innactiveFlag: boolean;
  @Column({ name: "idx" })
  public idx: number;
  @Column({ name: "name" })
  public name: string;
}
