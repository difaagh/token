import {
  BaseEntity,
  Column,
  PrimaryColumn,
  Entity,
  OneToOne,
  JoinColumn,
  ManyToOne,
  JoinTable,
  OneToMany,
} from "typeorm";
import { TokenTransactionConfigEntity } from "./tokenTransactionConfig";
import { TokenEntity } from "./token";
import Maintenable from "./maintenable";

@Entity({ name: "mnt_token_transaction" })
export class TokenTransactionEntity extends Maintenable {
  @PrimaryColumn({ name: "id" })
  public id: string;
  @Column({ name: "expired_date" })
  public expiredDate: Date;
  @Column({ name: "index" })
  public index: number;
  @Column({ name: "ref_no" })
  public refNo: string;
  @Column({ name: "delete_flag" })
  public deleteFlag: boolean;
  @Column({ name: "is_used" })
  public isUsed: boolean;
  @Column({ name: "factor_1" })
  public factor1: string;
  @Column({ name: "factor_2" })
  public factor2: string;
  @Column({ name: "factor_3" })
  public factor3: string;
  @Column({ name: "factor_4" })
  public factor4: string;
  @Column({ name: "factor_5" })
  public factor5: string;
  @Column({ name: "factor_6" })
  public factor6: string;
  @Column({ name: "factor_7" })
  public factor7: string;
  @Column({ name: "factor_8" })
  public factor8: string;
  @Column({ name: "factor_9" })
  public factor9: string;
  @Column({ name: "factor_10" })
  public factor10: string;
  // @Column({ name: "token_transaction_configuration" })
  // public tokenTransactionConfig: any;
  @OneToMany(() => TokenTransactionConfigEntity, (e) => e.id)
  @JoinColumn({ name: "id" })
  public tokenTransactionConfigId: TokenTransactionConfigEntity;
  @OneToMany(() => TokenEntity, (e) => e.id)
  @JoinColumn({ name: "id" })
  public tokenId: TokenEntity;
  // token
  @Column({ name: "token_id" })
  public TokenId: string;
  @Column({ name: "is_otp_token" })
  public isOtpToken: boolean;
  @Column({ name: "no_of_invalid" })
  public noOfInvalid: number;
  @Column({ name: "serial_no" })
  public serialNo: string;
  @Column({ name: "token_status" })
  public tokenStatus: boolean;
  @Column({ name: "user_id" })
  public userId: string;
  @Column({ name: "mobile_phone_no" })
  public mobilePhoneNo: string;

  // tokenTransactionConfig
  @Column({ name: "token_transaction_configuration_id" })
  public tokenTransactionConfigID: string;
  @Column({ name: "name" })
  public name: string;
  @Column({ name: "trx_code" })
  public trxCode: string;
}
