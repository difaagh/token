import {
  BaseEntity,
  Column,
  PrimaryColumn,
  Entity,
  OneToOne,
  JoinColumn,
  ManyToOne,
  JoinTable,
} from "typeorm";
import { TokenTransactionAuthEntity } from "./tokenTransactionAuth";
import { TokenTransactionEntity } from "./tokenTrasaction";
import Updatedable from "./updatedable";

@Entity({ name: "mnt_token_transaction_config" })
export class TokenTransactionConfigEntity extends Updatedable {
  @PrimaryColumn({ name: "id" })
  public id: string;
  @Column({ name: "index" })
  public index: number;
  @Column({ name: "name" })
  public name: string;
  @Column({ name: "trx_code" })
  public trxCode: string;
  @ManyToOne(() => TokenTransactionAuthEntity, (e) => e.id)
  @JoinColumn({ name: "id" })
  public tokenTransactionConfigId: TokenTransactionAuthEntity;
  @ManyToOne(() => TokenTransactionEntity, (e) => e.id)
  @JoinColumn({ name: "id" })
  public tokenTrasactionId: TokenTransactionEntity;
}
