import { Column, PrimaryColumn, Entity } from "typeorm";
import Updatedable from "./updatedable";

@Entity({ name: "mnt_token_configuration" })
export class TokenConfigEntity extends Updatedable {
  @PrimaryColumn({ name: "id" })
  public id: string;
  @Column({ name: "index" })
  public index: string;
  @Column({ name: "max_invalid_token" })
  public maxInvalidToken: number;
  @Column({ name: "validity" })
  public validity: number;
  @Column({ name: "token_length" })
  public tokenLength: string;
  @Column({ name: "delete_flag" })
  public deleteFlag: boolean;
}
