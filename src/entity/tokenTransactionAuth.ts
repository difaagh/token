import {
  BaseEntity,
  Column,
  PrimaryColumn,
  Entity,
  OneToMany,
  JoinTable,
} from "typeorm";
import { TokenTransactionConfigEntity } from "./tokenTransactionConfig";
import Updatedable from "./updatedable";

@Entity({ name: "mnt_token_transaction_auth" })
export class TokenTransactionAuthEntity extends Updatedable {
  @PrimaryColumn({ name: "id" })
  public id: string;
  @Column({ name: "delete_flag" })
  public deleteFlag: boolean;
  @Column({ name: "description" })
  public description: string;
  @Column({ name: "index" })
  public index: number;
  @Column({ name: "name" })
  public name: string;
  @Column({ name: "no_of_factor" })
  public noOfFactor: string;
  @OneToMany(() => TokenTransactionConfigEntity, (e) => e.id)
  public tokenTransactionConfigId: TokenTransactionConfigEntity;
}
