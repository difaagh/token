import {
  BaseEntity,
  PrimaryColumn,
  Entity,
  ManyToOne,
  JoinColumn,
} from "typeorm";
import { TemplateEntity } from "./template";

@Entity({ name: "not_template_parameter" })
export class TemplateParameterEntity extends BaseEntity {
  @PrimaryColumn({ name: "template_code" })
  public templateCode: string;
  @PrimaryColumn({ name: "identifier" })
  public identifier: string;
  @ManyToOne(() => TemplateEntity, (templateEntity) => templateEntity.code)
  @JoinColumn({ name: "template_code" })
  public template: TemplateEntity;
}
