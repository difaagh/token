import { BaseEntity, Column } from "typeorm";

export default class Updatedable extends BaseEntity {
  @Column({ name: "last_updated" })
  public lastUpdated: Date;
  @Column({ name: "updated_by" })
  public updatedBy: string;
  @Column({ name: "created_by" })
  public createdBy: string;
  @Column({ name: "date_created" })
  public dateCreated: Date;
}
