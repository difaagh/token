import {
  BaseEntity,
  Column,
  PrimaryColumn,
  Entity,
  OneToOne,
  JoinColumn,
  ManyToOne,
  JoinTable,
} from "typeorm";
import { TokenTransactionEntity } from "./tokenTrasaction";
import Updatedable from "./updatedable";

@Entity({ name: "mnt_token" })
export class TokenEntity extends Updatedable {
  @PrimaryColumn({ name: "id" })
  public id: string;
  @Column({ name: "index" })
  public index: number;
  @Column({ name: "is_otp_token" })
  public isOtpToken: boolean;
  @Column({ name: "no_of_invalid" })
  public noOfInvalid: number;
  @Column({ name: "serial_no" })
  public serialNo: string;
  @Column({ name: "token_status" })
  public tokenStatus: boolean;
  @Column({ name: "delete_flag" })
  public deleteFlag: boolean;
  @Column({ name: "user_id" })
  public userId: string;
  @Column({ name: "mobile_phone_no" })
  public mobilePhoneNo: string;
  @Column({ name: "date_created" })
  public dateCreated: Date;
  @ManyToOne(() => TokenTransactionEntity, (e) => e.id)
  @JoinColumn({ name: "id" })
  public tokenId: TokenTransactionEntity;
  // @OneToOne(() => TokenTransactionAuthEntity)
  // public tokenTransactionAuth: TokenTransactionAuthEntity;
}
