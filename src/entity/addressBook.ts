import {
  BaseEntity,
  Column,
  PrimaryColumn,
  Entity,
  OneToOne,
  JoinColumn,
  ManyToOne,
  JoinTable,
  PrimaryGeneratedColumn,
  Generated,
} from "typeorm";
import Maintenable from "./maintenable";

@Entity({ name: "not_addr_book" })
export class AddressBookEntity extends Maintenable {
  @PrimaryGeneratedColumn({ name: "id" })
  @Generated("uuid")
  public id: string;
  @Column({ name: "address" })
  public address: string;
  @Column({ name: "channel" })
  public channel: string;
  @Column({ name: "device_id" })
  public deviceId: string;
  @Column({ name: "device_typ" })
  public deviceType: string;
  @Column({ name: "user_id" })
  public userId: string;
  @Column({ name: "delete_flag" })
  public deleteFlag: boolean;
}
