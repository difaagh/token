import {
    ValidatorHeaderParam,
} from "le-validator";

export interface ICommonHeaderParamsProcessed {
    "content-language": "en" | "id";
    "tracer"?: "true" | "false";
}

export const commonHeaderParams: ValidatorHeaderParam[] = [{
    default: "en",
    enum: [ "en", "id"],
    name: "Content-Language",
    required: true,
    swagger: {
        description: "language ISO (en/th)",
        example: "en",
    },
    type: "string",
}, {
    enum: [ "true", "false"],
    name: "Tracer",
    required: false,
    swagger: {
        description: "Give payload info from pentacle",
        example: "true",
    },
    type: "string",
}];

