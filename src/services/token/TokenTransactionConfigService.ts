import { TokenTransactionConfigEntity } from "../../entity/tokenTransactionConfig";
import { Repository } from "typeorm";
import { IDI, TokenTransactionConfigInterface } from "../../interface";
import { PageableGetParam } from "../../lib";
import { Pagination } from "../../lib/helper/pagination";

// export default class TokenTransactionConfigService
//   implements TokenTransactionConfigInterface {
//   private tokenTransactionConfigRepository: Repository<
//     TokenTransactionConfigEntity
//   >;
//   constructor(di: IDI) {
//     this.tokenTransactionConfigRepository = di.appTypeOrm
//       .getConnection()
//       .getRepository(TokenTransactionConfigEntity);
//   }
//
//   private async findExits(id: string): Promise<TokenTransactionConfigEntity> {
//     return await this.tokenTransactionConfigRepository.findOne({
//       where: {
//         id,
//         deleteFlag: false,
//       },
//     });
//   }
//
// public async findAll(
//   query: PageableGetParam
// ): Promise<TokenTransactionConfigEntity[]> {
//   return await this.tokenTransactionConfigRepository.find(
//     Pagination.paginateOption(query, {
//       where: {
//         deleteFlag: false,
//       },
//     })
//   );
// }
//
//   public async findByTrxCodeAndDeleteFlagIsFalse(
//     trxCode: string
//   ): Promise<TokenTransactionConfigEntity> {
//     return await this.tokenTransactionConfigRepository
//       .findOne({
//         where: { trxCode, deleteFlag: false },
//       })
//       .catch((res) => {
//         if (res === undefined) return Promise.reject();
//       });
//   }
//
//   public getTokenTransactionConfigurationByTrxCode(trxCode: string) {
//     return this.findByTrxCodeAndDeleteFlagIsFalse(trxCode);
//   }
//
//   public async update(newToken: any) {
//     const token: TokenTransactionConfigEntity = await this.findByTrxCodeAndDeleteFlagIsFalse(
//       newToken.trxCode
//     );
//
//     if (token === null) {
//       throw new Error(`token transaction config not found ${newToken.trxCode}`);
//     }
//     if (newToken.index !== null || newToken.index !== undefined) {
//       token.index = newToken.index;
//     }
//     if (newToken.name !== null || newToken.name !== undefined) {
//       token.name = newToken.name;
//     }
//     if (newToken.trxCode !== null || newToken.trxCode !== undefined) {
//       token.trxCode = newToken.trxCode;
//     }
//     await this.tokenTransactionConfigRepository.save(token);
//     return token;
//   }
//
//   public async create(data: any): Promise<void> {
//     const config = await this.findExits(data.trxCode);
//     if (config !== undefined) {
//       throw new Error(`data already exists!`);
//     }
//     const newConfig = new TokenTransactionConfigEntity();
//     newConfig.id = data.id;
//     newConfig.trxCode = data.trxCode;
//     newConfig.name = data.name;
//     newConfig.trxCode = data.trxCode;
//
//     await this.tokenTransactionConfigRepository.save(newConfig);
//   }
// }

export default class TokenTransactionConfigService
  implements TokenTransactionConfigInterface {
  private tokenTransactionConfigRepository: Repository<
    TokenTransactionConfigEntity
  >;
  constructor(di: IDI) {
    this.tokenTransactionConfigRepository = di.appTypeOrm
      .getConnection()
      .getRepository(TokenTransactionConfigEntity);
  }
  // exists
  private async isExists(id: string): Promise<TokenTransactionConfigEntity> {
    return await this.tokenTransactionConfigRepository.findOne({
      where: { id, deleteFlag: false },
    });
  }
  // exists

  public async findByTrxCodeAndDeleteFlagIsFalse(
    trxCode: string
  ): Promise<TokenTransactionConfigEntity> {
    return await this.tokenTransactionConfigRepository.findOne({
      where: { trxCode },
    });
  }

  public async getTokenTransactionConfigurationByTrxCode(
    trxCode: string
  ): Promise<TokenTransactionConfigEntity> {
    return await this.tokenTransactionConfigRepository.findOne({
      where: { trxCode },
    });
  }

  // end implements

  public async create(data: any): Promise<TokenTransactionConfigEntity> {
    const exists = await this.tokenTransactionConfigRepository.findOne(data.id);
    if (exists) {
      throw new Error(`data already exists`);
    }
    const tokenTransactionConfig = new TokenTransactionConfigEntity();
    tokenTransactionConfig.id = data.id;
    tokenTransactionConfig.trxCode = data.trxCode;
    tokenTransactionConfig.name = data.name;
    tokenTransactionConfig.index = data.index;
    tokenTransactionConfig.dateCreated = new Date();
    tokenTransactionConfig.createdBy = "admin";

    try {
      return await this.tokenTransactionConfigRepository.save(
        tokenTransactionConfig
      );
    } catch (e) {
      throw new Error(`error ${e}`);
    }
  }

  public async findAll(
    query: PageableGetParam
  ): Promise<TokenTransactionConfigEntity[]> {
    return await this.tokenTransactionConfigRepository.find(
      Pagination.paginateOption(query, {
        where: {
          deleteFlag: false,
        },
      })
    );
  }

  public async update(newToken: any) {
    const token: TokenTransactionConfigEntity = await this.findByTrxCodeAndDeleteFlagIsFalse(
      newToken.id
    );

    if (token === null) {
      throw new Error(`token transaction config not found ${newToken.trxCode}`);
    }
    if (newToken.index !== null || newToken.index !== undefined) {
      token.index = newToken.index;
    }
    if (newToken.name !== null || newToken.name !== undefined) {
      token.name = newToken.name;
    }
    if (newToken.trxCode !== null || newToken.trxCode !== undefined) {
      token.trxCode = newToken.trxCode;
    }
    if (newToken.id !== null || newToken.id !== undefined) {
      token.id = newToken.id;
    }

    token.lastUpdated = new Date();
    token.updatedBy = "admin";
    await this.tokenTransactionConfigRepository.save(token);
    return token;
  }
}
