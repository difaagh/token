import { TokenConfigEntity } from "../../entity/tokenConfig";
import { IDI } from "../../interface";
import { Repository } from "typeorm";
import { PageableGetParam } from "../../lib";
import { Pagination } from "../../lib/helper/pagination";

export default class TokenConfigService {
  private tokenConfigRepository: Repository<TokenConfigEntity>;

  constructor(di: IDI) {
    this.tokenConfigRepository = di.appTypeOrm
      .getConnection()
      .getRepository(TokenConfigEntity);
  }
  public async findAll(query: PageableGetParam): Promise<TokenConfigEntity[]> {
    return await this.tokenConfigRepository.find(
      Pagination.paginateOption(query, {
        where: {
          deleteFlag: false,
        },
      })
    );
  }

  public async findByCodeWithDeleteFlag(
    id: string
  ): Promise<TokenConfigEntity> {
    const result = await this.tokenConfigRepository
      .findOne({
        where: {
          id,
          deleteFlag: false,
        },
      })
      .catch((err: any) => {
        if (err) {
          console.log(err);
          return err;
        }
      });
    return result;
  }

  public async create(data: any) {
    const exists = await this.findByCodeWithDeleteFlag(data.id);
    console.log(exists);
    if (exists) {
      throw new Error("Data already exists!");
    }

    const token = new TokenConfigEntity();
    token.dateCreated = new Date();
    token.createdBy = "admin";
    token.id = data.id;
    token.index = data.index;
    token.maxInvalidToken = data.maxInvalidToken;
    token.validity = data.validity;
    token.tokenLength = data.tokenLength;
    token.deleteFlag = false;

    console.log(token);

    try {
      return await this.tokenConfigRepository.save(token);
    } catch (e) {
      console.log(e);
      throw new Error(e);
    }
  }

  public async update(newToken: any) {
    const token = await this.findByCodeWithDeleteFlag(newToken.id);
    if (token == null) {
      throw new Error(`token config not found ${newToken.id}`);
    }

    if (
      newToken.maxInvalidToken !== null ||
      newToken.maxInvalidToken !== undefined
    ) {
      token.maxInvalidToken = newToken.maxInvalidToken;
    }
    if (newToken.validity !== null || newToken.validity !== undefined) {
      token.validity = newToken.validity;
    }
    if (newToken.tokenLength !== null || newToken.tokenLength !== undefined) {
      token.tokenLength = newToken.tokenLength;
    }
    if (newToken.index !== null || newToken.index !== undefined) {
      token.index = newToken.index;
    }

    token.lastUpdated = new Date();
    token.updatedBy = "Admin";
    return await this.tokenConfigRepository.save(token);
  }

  public async delete(id: string) {
    const token: TokenConfigEntity = await this.findByCodeWithDeleteFlag(id);
    if (token == null) {
      throw new Error(`token config not found: ${id}`);
    }
    token.deleteFlag = true;
    await this.tokenConfigRepository.save(token);
    return token;
  }
}
