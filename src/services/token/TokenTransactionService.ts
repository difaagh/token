import { TokenTransactionEntity } from "../../entity/tokenTrasaction";
import { Repository } from "typeorm";
import { IDI, TokenTransactionInterface } from "../../interface";
import { PageableGetParam } from "../../lib";
import { Pagination } from "../../lib/helper/pagination";

//   public async update(newToken: any) {
//     const token = await this.findByUserIdAndTokenStatusAndDeleteFlagIsFalse(
//       newToken.id
//     );
//     if (token === null) {
//       throw new Error(`token transaction not found ${newToken.id}`);
//     }
//     if (newToken.index !== null || newToken.index !== undefined) {
//       token.index = newToken.index;
//     }
//     if (newToken.expiredDate !== null || newToken.expiredDate) {
//       token.expiredDate = new Date();
//     }
//     if (newToken.refNo !== null || newToken.refNo !== undefined) {
//       token.refNo = newToken.refNo;
//     }
//     if (newToken.isUsed !== null || newToken.isUsed !== undefined) {
//       token.isUsed = newToken.isUsed;
//     }
//     if (newToken.factor1 !== null || newToken.factor1 !== undefined) {
//       token.factor1 = newToken.factor1;
//     }
//     if (newToken.factor2 !== null || newToken.factor2 !== undefined) {
//       token.factor2 = newToken.factor2;
//     }
//     if (newToken.factor3 !== null || newToken.factor3 !== undefined) {
//       token.factor3 = newToken.factor3;
//     }
//     if (newToken.factor4 !== null || newToken.factor4 !== undefined) {
//       token.factor4 = newToken.factor4;
//     }
//     if (newToken.factor5 !== null || newToken.factor5 !== undefined) {
//       token.factor5 = newToken.factor5;
//     }
//     if (newToken.factor6 !== null || newToken.factor6 !== undefined) {
//       token.factor6 = newToken.factor6;
//     }
//     if (newToken.factor7 !== null || newToken.factor7 !== undefined) {
//       token.factor7 = newToken.factor7;
//     }
//     if (newToken.factor8 !== null || newToken.factor8 !== undefined) {
//       token.factor8 = newToken.factor8;
//     }
//     if (newToken.factor9 !== null || newToken.factor9 !== undefined) {
//       token.factor9 = newToken.factor9;
//     }
//     if (newToken.factor10 !== null || newToken.factor10 !== undefined) {
//       token.factor10 = newToken.factor10;
//     }
//     if (newToken.expiredDate !== null || newToken.expiredDate !== undefined) {
//       token.expiredDate = newToken.expiredDate;
//     }
//     await this.tokenTransactionConfigRepository.save(token);
//     return token;
//   }

export default class TokenTrasactionService
  implements TokenTransactionInterface {
  private tokenTransactionConfigRepository: Repository<TokenTransactionEntity>;
  constructor(di: IDI) {
    this.tokenTransactionConfigRepository = di.appTypeOrm
      .getConnection()
      .getRepository(TokenTransactionEntity);
  }

  private async isExists(id: string): Promise<TokenTransactionEntity> {
    return await this.tokenTransactionConfigRepository.findOne({
      where: { id, deleteFlag: false },
    });
  }

  public async findByRefNoAndDeleteFlagIsFalse(
    refNo: string
  ): Promise<TokenTransactionEntity> {
    return await this.tokenTransactionConfigRepository.findOne({
      where: { refNo, deleteFlag: false },
    });
  }

  public async create(data: any): Promise<TokenTransactionEntity> {
    const exists = await this.isExists(data.id);
    console.log(exists);
    if (exists) {
      throw new Error("Data already exists!");
    }

    const token = new TokenTransactionEntity();
    token.id = data.id;
    token.index = data.index;
    token.refNo = data.refNo;
    token.expiredDate = new Date();
    token.factor1 = data.factor1;
    token.factor2 = data.factor2;
    token.factor3 = data.factor3;
    token.factor4 = data.factor4;
    token.factor5 = data.factor5;
    token.factor6 = data.factor6;
    token.factor7 = data.factor7;
    token.factor8 = data.factor8;
    token.factor9 = data.factor9;
    token.factor10 = data.factor10;

    console.log(token);

    try {
      return await this.tokenTransactionConfigRepository.save(token);
    } catch (e) {
      console.log(e);
      throw new Error(e);
    }
  }

  // end implements

  public async delete(id: string) {
    const token: TokenTransactionEntity = await this.isExists(id);
    if (token == null) {
      throw new Error(`token config not found: ${id}`);
    }
    token.deleteFlag = true;
    await this.tokenTransactionConfigRepository.save(token);
    return token;
  }
  public async update(newToken: any) {
    const token = await this.isExists(newToken.id);
    if (token === null) {
      throw new Error(`token transaction not found ${newToken.id}`);
    }
    if (newToken.index !== null || newToken.index !== undefined) {
      token.index = newToken.index;
    }
    if (newToken.expiredDate !== null || newToken.expiredDate) {
      token.expiredDate = new Date();
    }
    if (newToken.refNo !== null || newToken.refNo !== undefined) {
      token.refNo = newToken.refNo;
    }
    if (newToken.isUsed !== null || newToken.isUsed !== undefined) {
      token.isUsed = newToken.isUsed;
    }
    if (newToken.factor1 !== null || newToken.factor1 !== undefined) {
      token.factor1 = newToken.factor1;
    }
    if (newToken.factor2 !== null || newToken.factor2 !== undefined) {
      token.factor2 = newToken.factor2;
    }
    if (newToken.factor3 !== null || newToken.factor3 !== undefined) {
      token.factor3 = newToken.factor3;
    }
    if (newToken.factor4 !== null || newToken.factor4 !== undefined) {
      token.factor4 = newToken.factor4;
    }
    if (newToken.factor5 !== null || newToken.factor5 !== undefined) {
      token.factor5 = newToken.factor5;
    }
    if (newToken.factor6 !== null || newToken.factor6 !== undefined) {
      token.factor6 = newToken.factor6;
    }
    if (newToken.factor7 !== null || newToken.factor7 !== undefined) {
      token.factor7 = newToken.factor7;
    }
    if (newToken.factor8 !== null || newToken.factor8 !== undefined) {
      token.factor8 = newToken.factor8;
    }
    if (newToken.factor9 !== null || newToken.factor9 !== undefined) {
      token.factor9 = newToken.factor9;
    }
    if (newToken.factor10 !== null || newToken.factor10 !== undefined) {
      token.factor10 = newToken.factor10;
    }
    if (newToken.expiredDate !== null || newToken.expiredDate !== undefined) {
      token.expiredDate = newToken.expiredDate;
    }
    await this.tokenTransactionConfigRepository.save(token);
    return token;
  }
  public async findAll(
    query: PageableGetParam
  ): Promise<TokenTransactionEntity[]> {
    return await this.tokenTransactionConfigRepository.find(
      Pagination.paginateOption(query, {
        where: {
          deleteFlag: false,
        },
      })
    );
  }
}
