import { TokenServiceInterface } from "../../interface";
import { TokenEntity } from "../../entity/token";
import { Repository } from "typeorm";
import { formatter } from "../../lib/helper/formatter";
import { TokenStatus } from "../../enum";
import { PageableGetParam } from "../../lib";
import { IDI } from "../../interface";
import { Pagination } from "../../lib/helper/pagination";

// export default class TokenService implements TokenServiceInterface {
//   private tokenRepository: Repository<TokenEntity>;
//
//   constructor(di: IDI) {
//     this.tokenRepository = di.appTypeOrm
//       .getConnection()
//       .getRepository(TokenEntity);
//   }
//
//   public async findAll(query: PageableGetParam): Promise<TokenEntity[]> {
//     return await this.tokenRepository.find(
//       Pagination.paginateOption(query, {
//         where: {
//           deleteFlag: false,
//         },
//       })
//     );
//   }
//
//   public generateReferenceNo(bulk: number): number {
//     const query = this.tokenRepository.find({
//       select: ["index"],
//       where: { index: bulk },
//     });
//     const num = Number(query.toString());
//
//     return num;
//   }
//
//   public generateToken(factorList: string[], tokenLength: number): string {
//     let tokenString: string = "";
//     const regex = "\\D+";
//
//     factorList.forEach((factor) => {
//       tokenString += factor;
//     });
//     const reversed = String(tokenString).split("").reverse().join("");
//     const byteOne = tokenString.split("").map((s) => s.charCodeAt(0));
//     const byteTwo = reversed.split("").map((s) => s.charCodeAt(0));
//
//     for (let i = 0; i < byteOne.length; i++) {
//       byteOne[i] = (<number>(3 * byteOne[i] - 2 * byteTwo[i])) | 0;
//       byteTwo[i] = (<number>(4 * byteTwo[i] - 3 * byteOne[i])) | 0;
//     }
//     let randomStrOne: string = String.fromCharCode
//       .apply(null, byteOne)
//       .replace(new RegExp(regex, "g"), "");
//     let randomStrTwo: string = String.fromCharCode
//       .apply(null, byteTwo)
//       .replace(new RegExp(regex, "g"), "");
//
//     for (const i of factorList.keys()) {
//       const factor = factorList[i];
//       {
//         randomStrOne += factor.replace(new RegExp(regex, "g"), "");
//         randomStrTwo += factor.replace(new RegExp(regex, "g"), "");
//       }
//     }
//
//     const reversed1 = randomStrOne.toString().split("").reverse().join("");
//     const reversed2 = randomStrTwo.toString().split("").reverse().join("");
//     const biOne = BigInt(reversed1 + randomStrTwo);
//     const biTwo = BigInt(randomStrOne + reversed2);
//
//     let biThree = biOne + biTwo;
//     biThree = biThree * biTwo;
//     biThree = biThree + biTwo;
//     biThree = biThree / biOne;
//     tokenString = biThree.toString();
//     return tokenString.substring(0, tokenLength);
//   }
//
//   private generateTokenSerialNo(): string {
//     const serialNumSB: string = formatter();
//     const random: number[] = [];
//     for (let ix: number = 0; ix < 4; ix++) {
//       random.push(Math.random() * 10);
//     }
//     const serialRandom: string = serialNumSB.concat(
//       random.toString().replace(/,/g, "")
//     );
//
//     return serialRandom;
//   }
//   public async findByUserIdAndTokenStatusAndDeleteFlagIsFalse(
//     userId: string,
//     tokenStatus?: TokenStatus
//   ): Promise<TokenEntity> {
//     if (tokenStatus) {
//       return await this.tokenRepository.findOne({
//         where: { userId, deleteFlag: false, tokenStatus },
//       });
//     } else {
//       return await this.tokenRepository.findOne({
//         where: { userId, deleteFlag: false },
//       });
//     }
//   }
//
//   public async registerToken(
//     userId: string,
//     mobileNo: string
//   ): Promise<TokenEntity> {
//     const exists = await this.findByUserIdAndTokenStatusAndDeleteFlagIsFalse(
//       userId
//     );
//     console.log(exists);
//     if (exists) {
//       throw new Error("Data already exists!");
//     }
//
//     const token = new TokenEntity();
//     token.mobilePhoneNo = mobileNo;
//     token.userId = userId;
//     token.noOfInvalid = 0;
//     token.serialNo = this.generateTokenSerialNo();
//     token.dateCreated = new Date();
//     token.tokenStatus = true;
//     return await this.tokenRepository.save(token);
//   }
//
//   public getTokenByUserId(userId: string) {
//     return this.findByUserIdAndTokenStatusAndDeleteFlagIsFalse(
//       userId,
//       TokenStatus.ACCTIVE
//     );
//   }
//
//   public async delete(userId: string) {
//     const token: TokenEntity = await this.findByUserIdAndTokenStatusAndDeleteFlagIsFalse(
//       userId
//     );
//     if (token === undefined) {
//       throw new Error(`userId not found ${userId}`);
//     }
//     token.deleteFlag = true;
//     await this.tokenRepository.save(token);
//     return token;
//   }
//
//   public async update(newToken: any) {
//     const token = await this.findByUserIdAndTokenStatusAndDeleteFlagIsFalse(
//       newToken.id
//     );
//     if (token === null) {
//       throw new Error(`token not found ${newToken.id}`);
//     }
//     if (newToken.index !== null || newToken.index !== undefined) {
//       token.index = newToken.name;
//     }
//     if (newToken.isOtpToken !== null || newToken.isOtpToken !== undefined) {
//       token.isOtpToken = newToken.isOtpToken;
//     }
//     if (newToken.noOfInvalid !== null || newToken.noOfInvalid !== undefined) {
//       token.noOfInvalid = newToken.noOfInvalid;
//     }
//     if (newToken.serialNo !== null || newToken.serialNo !== undefined) {
//       token.serialNo = newToken.serialNo;
//     }
//     if (newToken.tokenStatus !== null || newToken.tokenStatus !== undefined) {
//       token.tokenStatus = newToken.tokenStatus;
//     }
//     // if (newToken.deleteFlag !== null || newToken.deleteFlag !== undefined){
//     //     token.deleteFlag = newToken.deleteFlag;
//     // }
//     if (newToken.userId !== null || newToken.userId !== undefined) {
//       token.userId = newToken.userId;
//     }
//     if (newToken.userId !== null || newToken.userId !== undefined) {
//       token.userId = newToken.userId;
//     }
//     // if (newToken.dateCreated !== null || newToken.dateCreated !== undefined) {
//     //   token.dateCreated = newToken.dateCreated;
//     // }
//     await this.tokenRepository.save(token);
//     return token;
//   }
// }

export default class TokenService implements TokenServiceInterface {
  private tokenRepository: Repository<TokenEntity>;

  constructor(di: IDI) {
    this.tokenRepository = di.appTypeOrm
      .getConnection()
      .getRepository(TokenEntity);
  }
  //
  private async isExists(id: string): Promise<TokenEntity> {
    return await this.tokenRepository.findOne({
      where: { id, deleteFlag: false },
    });
  }
  //

  public async registerToken(userId: string, mobileNo: string): Promise<void> {
    const exists = await this.isExists(userId);
    if (exists) {
      throw new Error("Data already exists!");
    }

    const token = new TokenEntity();
    token.mobilePhoneNo = mobileNo;
    token.userId = userId;
    token.noOfInvalid = 0;
    token.serialNo = this.generateTokenSerialNo();
    token.dateCreated = new Date();
    token.tokenStatus = true;
    await this.tokenRepository.save(token);
  }

  public async findByUserIdAndTokenStatusAndDeleteFlagIsFalse(
    userId: string,
    tokenStatus: any
  ): Promise<TokenEntity> {
    return await this.tokenRepository.findOne({
      where: {
        userId,
        tokenStatus,
        deleteFlag: false,
      },
    });
  }

  public async getTokenByUserId(userId: string): Promise<TokenEntity> {
    return await this.tokenRepository.findOne({
      where: {
        userId,
        tokenStatus: true,
      },
    });
  }

  public async generateReferenceNo(bulk: number): Promise<number> {
    const query = await this.tokenRepository.findOne({
      where: {
        index: bulk,
      },
    });
    const numbers: number = query.index;
    return numbers;
  }

  // end interface

  private generateTokenSerialNo(): string {
    const serialNumSB: string = formatter();
    const random: number[] = [];
    for (let ix: number = 0; ix < 4; ix++) {
      random.push(Math.random() * 10);
    }
    const serialRandom: string = serialNumSB.concat(
      random.toString().replace(/,/g, "")
    );

    return serialRandom;
  }

  public async findAll(query: PageableGetParam): Promise<TokenEntity[]> {
    return await this.tokenRepository.find(
      Pagination.paginateOption(query, {
        where: {
          deleteFlag: false,
        },
      })
    );
  }

  public async update(newToken: any) {
    const token = await this.isExists(newToken.id);
    if (token === null) {
      throw new Error(`token not found ${newToken.id}`);
    }
    if (newToken.index !== null || newToken.index !== undefined) {
      token.index = newToken.name;
    }
    if (newToken.isOtpToken !== null || newToken.isOtpToken !== undefined) {
      token.isOtpToken = newToken.isOtpToken;
    }
    if (newToken.noOfInvalid !== null || newToken.noOfInvalid !== undefined) {
      token.noOfInvalid = newToken.noOfInvalid;
    }
    if (newToken.serialNo !== null || newToken.serialNo !== undefined) {
      token.serialNo = newToken.serialNo;
    }
    if (newToken.tokenStatus !== null || newToken.tokenStatus !== undefined) {
      token.tokenStatus = newToken.tokenStatus;
    }
    if (newToken.userId !== null || newToken.userId !== undefined) {
      token.userId = newToken.userId;
    }
    if (newToken.userId !== null || newToken.userId !== undefined) {
      token.userId = newToken.userId;
    }
    // if (newToken.dateCreated !== null || newToken.dateCreated !== undefined) {
    //   token.dateCreated = newToken.dateCreated;
    // }
    await this.tokenRepository.save(token);
    return token;
  }
  public async delete(id: string) {
    const token: TokenEntity = await this.isExists(id);
    if (token == null) {
      throw new Error(`token config not found: ${id}`);
    }
    token.deleteFlag = true;
    await this.tokenRepository.save(token);
    return token;
  }
}
