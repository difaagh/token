import { TokenTransactionAuthEntity } from "../../entity/tokenTransactionAuth";
import { Repository } from "typeorm";
import { IDI } from "../../interface";
import { PageableGetParam } from "../../lib";
import { Pagination } from "../../lib/helper/pagination";

export default class TokenTransactionAuthService {
  private tokenTransactionConfigRepository: Repository<
    TokenTransactionAuthEntity
  >;
  constructor(di: IDI) {
    this.tokenTransactionConfigRepository = di.appTypeOrm
      .getConnection()
      .getRepository(TokenTransactionAuthEntity);
  }
  public async findAll(
    query: PageableGetParam
  ): Promise<TokenTransactionAuthEntity[]> {
    return await this.tokenTransactionConfigRepository.find(
      Pagination.paginateOption(query, {
        where: {
          deleteFlag: false,
        },
      })
    );
  }

  public async findByIdithDeleteFlag(
    id: string
  ): Promise<TokenTransactionAuthEntity> {
    return await this.tokenTransactionConfigRepository.findOne({
      where: {
        id,
        deleteFlag: false,
      },
    });
  }

  public async create(data: any) {
    const exists = await this.findByIdithDeleteFlag(data.id);
    console.log(exists);
    if (exists) {
      throw new Error("Data already exists!");
    }

    const token = new TokenTransactionAuthEntity();
    token.id = data.id;
    token.index = data.index;
    token.name = data.name;
    token.noOfFactor = data.noOfFactor;
    token.description = data.description;

    console.log(token);

    try {
      return await this.tokenTransactionConfigRepository.save(token);
    } catch (e) {
      console.log(e);
      throw new Error(e);
    }
  }

  public async update(newToken: any) {
    const token = await this.findByIdithDeleteFlag(newToken.id);
    if (token == null) {
      throw new Error(`token Transaction auth not found ${newToken.id}`);
    }

    if (newToken.index !== null || newToken.index !== undefined) {
      token.index = newToken.index;
    }
    if (newToken.name !== null || newToken.name !== undefined) {
      token.name = newToken.name;
    }
    if (newToken.description !== null || newToken.description !== undefined) {
      token.description = newToken.description;
    }
    if (newToken.noOfFactor !== null || newToken.noOfFactor !== undefined) {
      token.noOfFactor = newToken.noOfFactor;
    }

    await this.tokenTransactionConfigRepository.save(token);
    return token;
  }

  public async delete(id: string) {
    const token: TokenTransactionAuthEntity = await this.findByIdithDeleteFlag(
      id
    );
    if (token == null) {
      throw new Error(`token transaction auth not found: ${id}`);
    }
    token.deleteFlag = true;
    await this.tokenTransactionConfigRepository.save(token);
    return token;
  }
}
