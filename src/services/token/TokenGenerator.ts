export default class TokenGenerator {
  public generateToken(factorList: string[], tokenLength: number): string {
    let tokenString: string = "";
    const regex = "\\D+";

    factorList.forEach((factor) => {
      tokenString += factor;
    });
    const reversed = String(tokenString).split("").reverse().join("");
    const byteOne = tokenString.split("").map((s) => s.charCodeAt(0));
    const byteTwo = reversed.split("").map((s) => s.charCodeAt(0));

    for (let i = 0; i < byteOne.length; i++) {
      byteOne[i] = (<number>(3 * byteOne[i] - 2 * byteTwo[i])) | 0;
      byteTwo[i] = (<number>(4 * byteTwo[i] - 3 * byteOne[i])) | 0;
    }
    let randomStrOne: string = String.fromCharCode
      .apply(null, byteOne)
      .replace(new RegExp(regex, "g"), "");
    let randomStrTwo: string = String.fromCharCode
      .apply(null, byteTwo)
      .replace(new RegExp(regex, "g"), "");

    for (const i of factorList.keys()) {
      const factor = factorList[i];
      {
        randomStrOne += factor.replace(new RegExp(regex, "g"), "");
        randomStrTwo += factor.replace(new RegExp(regex, "g"), "");
      }
    }

    const reversed1 = randomStrOne.toString().split("").reverse().join("");
    const reversed2 = randomStrTwo.toString().split("").reverse().join("");
    const biOne = BigInt(reversed1 + randomStrTwo);
    const biTwo = BigInt(randomStrOne + reversed2);

    let biThree = biOne + biTwo;
    biThree = biThree * biTwo;
    biThree = biThree + biTwo;
    biThree = biThree / biOne;
    tokenString = biThree.toString();
    return tokenString.substring(0, tokenLength);
  }
}
