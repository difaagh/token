import {
  TokenEntity,
  TokenConfigEntity,
  TokenTransactionEntity,
  TokenTransactionConfigEntity,
} from "../../entity";
import { sha256 } from "js-sha256";
import { IDI } from "../../interface";
import TokenService from "../token/TokenService";
import TokenTransactionConfigService from "../token/TokenTransactionConfigService";
import TokenTrasactionService from "../token/TokenTransactionService";
import TokenGenerator from "../token/TokenGenerator";
import { Repository } from "typeorm";

export default class MainService {
  private tokenService: TokenService;
  private tokenTransactionConfigurationService: TokenTransactionConfigService;
  private tokenTransactionService: TokenTrasactionService;
  private tokenGenerator: TokenGenerator;
  private tokenTransactionRepo: Repository<TokenTransactionEntity>;
  constructor(di: IDI) {
    this.tokenService = new TokenService(di);
    this.tokenTransactionConfigurationService = new TokenTransactionConfigService(
      di
    );
    this.tokenTransactionService = new TokenTrasactionService(di);
    this.tokenTransactionRepo = di.appTypeOrm
      .getConnection()
      .getRepository(TokenTransactionEntity);
    this.tokenGenerator = new TokenGenerator();
  }
  private async generate(
    userId: string,
    transactionCode: string,
    factorList: string[]
  ): Promise<string> {
    const newToken = await this.tokenService.getTokenByUserId(userId);
    const tokenTransacConfig = await this.tokenTransactionConfigurationService.getTokenTransactionConfigurationByTrxCode(
      transactionCode
    );

    const token = this.tokenGenerator.generateToken(
      this.hasingContent(factorList.join("")),
      6
    );
    console.log("token === ", token);

    factorList = this.fillUpFactorList(factorList);
    const refNo: string = this.getTransactionSequenceNo();

    const tokenTransaction = new TokenTransactionEntity();
    // token
    tokenTransaction.TokenId = newToken.id;
    tokenTransaction.isOtpToken = newToken.isOtpToken;
    tokenTransaction.noOfInvalid = newToken.noOfInvalid;
    tokenTransaction.serialNo = newToken.serialNo;
    tokenTransaction.tokenStatus = newToken.tokenStatus;
    tokenTransaction.userId = newToken.userId;
    tokenTransaction.mobilePhoneNo = newToken.mobilePhoneNo;
    // tokenTransactionConfig
    tokenTransaction.tokenTransactionConfigID = tokenTransacConfig.id;
    tokenTransaction.name = tokenTransacConfig.name;
    tokenTransaction.trxCode = tokenTransacConfig.trxCode;

    // tokenTransaction
    tokenTransaction.refNo = refNo;
    tokenTransaction.dateCreated = new Date();
    tokenTransaction.factor1 = factorList[0];
    tokenTransaction.factor2 = factorList[1];
    tokenTransaction.factor3 = factorList[2];
    tokenTransaction.factor4 = factorList[3];
    tokenTransaction.factor5 = factorList[4];
    tokenTransaction.factor6 = factorList[5];
    tokenTransaction.factor7 = factorList[6];
    tokenTransaction.factor8 = factorList[7];
    tokenTransaction.factor9 = factorList[8];
    tokenTransaction.factor10 = factorList[9];
    tokenTransaction.isUsed = true;

    try {
      await this.tokenTransactionRepo.save(tokenTransaction);
      return token;
    } catch (e) {
      return `error ${e}`;
    }
  }

  private sha2(input: string): string {
    const hash: string = sha256(input);
    return hash;
  }

  private fillUpFactorList(factorList: string[]): string[] {
    const i: number = factorList.length;
    const factor: string[] = [];

    if (i < 10) {
      for (let j = i; j < 10; j++) {
        factor.push("");
      }
    }
    return factor;
  }

  private hasingContent(content: string): string[] {
    const messageHash: string = this.sha2(content);
    const data: string[] = [];
    const field1 = messageHash.substring(0, 14);
    const field2 = messageHash.substring(14, 27);
    const field3 = messageHash.substring(27, 40);
    const field4 = messageHash.substring(40, 54);
    const field5 = messageHash.substring(54, 64);
    data.push(field1);
    data.push(field2);
    data.push(field3);
    data.push(field4);
    data.push(field5);
    return data;
  }
  //
  public generateTokenSequenceNo(bulk: number): string[] {
    const sequence = bulk;
    console.log("sequence === ", sequence);
    const sequenceList: string[] = [];
    const sb: string = "";
    for (let i = 0; i < bulk; i++) {
      sb.concat(this.format(sequence - i, 6));
      console.log("sb = ", sb);
      sequenceList.push(sb);
      console.log("sequenceList = ", sequenceList);
    }
    return sequenceList;
  }

  public getTransactionSequenceNo(): string {
    const sequence: string[] = this.generateTokenSequenceNo(1);

    return sequence[0];
  }

  private format(num: number, lengthh: number): string {
    const str: string = num.toString();
    if (str.length > lengthh) return str;
    return (
      "0000000000000000000000000000000000".substring(0, lengthh - str.length) +
      str
    );
  }
  public async validate(
    userId: string,
    transactionCode: string,
    tokenSerialNo: string,
    factorList: string[],
    tokenResponse: string
  ): Promise<void> {
    const originalToken: string = await this.generate(
      userId,
      transactionCode,
      factorList
    );
    if (tokenResponse !== originalToken) {
      throw Promise.reject();
    }
    // if (!tokenResponse.)
  }
}
