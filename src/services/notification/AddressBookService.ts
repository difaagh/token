import { AddressBookEntity } from "../../entity/addressBook";
import { IDI, AddressBookInterface } from "../../interface";
import { Repository } from "typeorm";
import { PageableGetParam } from "../../lib";
import { Pagination } from "../../lib/helper/pagination";

export default class AddressBookService implements AddressBookInterface {
  private addressBookRepository: Repository<AddressBookEntity>;

  constructor(di: IDI) {
    this.addressBookRepository = di.appTypeOrm
      .getConnection()
      .getRepository(AddressBookEntity);
  }

  public async findAll(query: PageableGetParam): Promise<AddressBookEntity[]> {
    return await this.addressBookRepository.find(
      Pagination.paginateOption(query, {
        where: {
          deleteFlag: false,
        },
      })
    );
  }

  private async findExits(id: string): Promise<AddressBookEntity> {
    return await this.addressBookRepository.findOne({
      where: {
        id,
        deleteFlag: false,
      },
    });
  }

  public async getAddressBook(userId: string, channel?: string) {
    if (channel) {
      return await this.findByUserIdAndChannel(userId, channel);
    } else {
      return await this.findByUserId(userId);
    }
  }

  public async findByUserId(userId: string): Promise<AddressBookEntity[]> {
    return await this.addressBookRepository.find({
      where: {
        userId,
        deleteFlag: false,
      },
    });
  }

  public async findByUserIdAndChannel(
    userId: string,
    channel: string
  ): Promise<AddressBookEntity[]> {
    console.log("userId dan channel", userId, " ", channel);
    return await this.addressBookRepository.find({
      where: {
        userId,
        channel,
        deleteFlag: false,
      },
    });
  }

  public async findByDeviceIdAndChannel(
    deviceId: string,
    channel: string
  ): Promise<AddressBookEntity> {
    console.log("channel === ", channel);
    console.log("deviceId === ", deviceId);
    return await this.addressBookRepository.findOne({
      where: {
        deviceId,
        channel,
        deleteFlag: false,
      },
    });
  }

  public async create(newAddressBook: any): Promise<void> {
    const exists = await this.findExits(newAddressBook.id);
    if (exists) {
      throw new Error(`data already exists!`);
    }
    const addressBook = new AddressBookEntity();
    addressBook.address = newAddressBook.address;
    addressBook.channel = newAddressBook.channel;
    addressBook.deviceId = newAddressBook.deviceId;
    addressBook.deviceType = newAddressBook.deviceType;
    addressBook.userId = newAddressBook.userId;
    addressBook.createdBy = "admin";
    addressBook.dateCreated = new Date();
    addressBook.deleteFlag = false;
    try {
      await this.addressBookRepository.save(addressBook);
    } catch (e) {
      console.log(e);
      throw new Error(e);
    }
  }

  // public async delete(
  //
  // ): Promise<void> {
  //   // soft delete
  //   addressBook.deleteFlag = true;
  //   await this.addressBookRepository.save(addressBook);
  // }

  public async deleteByUserId(userId: string): Promise<void> {
    // const exists: AddressBookEntity[] = await this.findByUserId(userId);
    const exists: AddressBookEntity = await this.findByUserIdWithDeleteFlagIsFalse(
      userId
    );
    if (exists) {
      exists.deleteFlag = true;
      await this.addressBookRepository.save(exists);
    } else {
      throw new Error(`userId not found ${userId}`);
    }
    // soft delete
  }

  public async deleteByUserIdAndChannel(
    userId: string,
    channel: string
  ): Promise<void> {
    // const exists: AddressBookEntity = await this.findByUserIdAndChannel(
    //   userId,
    //   channel
    // );
    const exists: AddressBookEntity = await this.findByUserIdWithDeleteFlagIsFalse(
      userId,
      channel
    );
    // soft delete
    if (exists) {
      exists.deleteFlag = true;
      await this.addressBookRepository.save(exists);
    } else {
      throw new Error(`userId and channel not found ${userId}, ${channel}`);
    }
  }

  // use for soft delete
  private async findByUserIdWithDeleteFlagIsFalse(
    userId: string,
    channel?: string
  ): Promise<AddressBookEntity> {
    if (channel) {
      return await this.addressBookRepository.findOne({
        where: {
          userId,
          channel,
          deleteFlag: false,
        },
      });
    } else {
      return await this.addressBookRepository.findOne({
        where: {
          userId,
          deleteFlag: false,
        },
      });
    }
  }
}
