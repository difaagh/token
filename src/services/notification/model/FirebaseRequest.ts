import { FirebaseData } from "./FirebaseData";

export class FirebaseRequest {
  to: string;
  data: FirebaseData;
}
