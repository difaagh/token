import { NotificationEngine } from "../../interface";
import { NotificationDto } from "../../dto/NotificationDto";
import { FirebaseRequest } from "./model/FirebaseRequest";
import { FirebaseData } from "./model/FirebaseData";
import * as admin from "firebase-admin";

export default class FirebaseNotificationEngine implements NotificationEngine {
  public async sendNotification(
    notificationDto: NotificationDto
  ): Promise<string> {
    const request = new FirebaseRequest();
    request.to = notificationDto.to;
    const data = new FirebaseData();
    data.title = notificationDto.subject;
    data.detail = this.toJson(notificationDto);
    request.data = data;

    const message = {
      notification: {
        title: data.title,
        body: data.detail,
      },
    };
    admin
      .messaging()
      .sendToDevice(notificationDto.to, message)
      .then((res) => console.log("success send ", res))
      .catch((err) => console.log(`error send message ${err}`));
    return;
  }

  public unsubscribe(address: string): Promise<void> {
    return;
  }

  public async subscribe(
    userId: string,
    deviceType: string,
    deviceId: string
  ): Promise<string> {
    console.log(deviceId);
    return deviceId;
  }

  private toJson(notificationDto: NotificationDto): string {
    const payload = new Map();
    payload.set("message", notificationDto.message);
    payload.set("value", notificationDto.valueMap);
    return JSON.stringify([...payload]);
  }
}
