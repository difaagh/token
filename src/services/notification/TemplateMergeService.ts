import TemplateChannelService from "../template/TemplateChannelService";
import { IDI, TemplateMergeInterface } from "../../interface";
import { Repository } from "typeorm";
import MergeResult from "../../dto/MergeResult";
import { TemplateChannelEntity } from "../../entity";

export default class TemplateMergeService implements TemplateMergeInterface {
  private templateChannelService: TemplateChannelService;
  constructor(di: IDI) {
    this.templateChannelService = new TemplateChannelService(di);
  }

  public async merge(
    templateCode: string,
    channel: string,
    valueMap: any
  ): Promise<MergeResult> {
    const templateChannel: TemplateChannelEntity = await this.templateChannelService.findByTemplateCodeAndChannel(
      templateCode,
      channel
    );
    return this.getMergeResult(templateChannel, valueMap);
  }

  public async merges(
    templateCode: string,
    valueMap: any
  ): Promise<MergeResult[]> {
    console.log("template code === ", templateCode);
    console.log("valueMap ===", valueMap);
    const arr: MergeResult[] = [];
    const templateChannel = await this.templateChannelService.findByTemplateCode(
      templateCode
    );
    console.log("hasil templateChannel === ", templateChannel);
    templateChannel.map(async (templatesChannel) => {
      arr.push(await this.getMergeResult(templatesChannel, valueMap));
    });
    return arr;
  }

  private async getMergeResult(
    templateChannel: TemplateChannelEntity,
    valueMap: any
  ): Promise<MergeResult> {
    const builder: string = templateChannel.channel;
    console.log("builder === ", builder);
    console.log("valueMap === ", valueMap);
    const builder1 = builder.toString();
    const builder2: string = this.mergeStringLoop(builder1, valueMap);
    console.log("builder  === ", builder);
    console.log("builder1  === ", builder2);
    console.log("builder2  === ", builder1);
    const merge = new MergeResult();
    merge.channel = templateChannel.channel;
    merge.message = builder2; // in java (StringEscapeUtils.unescapeHtml4(builder.toString()))
    merge.subject = templateChannel.subject;
    console.log("merge === ", merge);
    return merge;
  }
  private mergeStringLoop(toMerge: string, forLoop: any): string {
    let strAfterMerge: string = "";
    for (const [key, value] of forLoop) {
      if (strAfterMerge.length === 0) {
        strAfterMerge = toMerge.replace("$" + key, value);
      }
    }
    return strAfterMerge;
  }
}
