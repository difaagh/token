import {
  IDI,
  NotificationInterface,
  NotificationEngine,
} from "../../interface";
import NotificationRequest from "../../dto/NotificationRequest";
import { AddressBookEntity } from "../../entity";
import AddressBookService from "./AddressBookService";
import TemplateMergeService from "./TemplateMergeService";
import { NotificationDto } from "../../dto/NotificationDto";
import MergeResult from "../../dto/MergeResult";
import { Repository } from "typeorm";
import FirebaseNotificationEngine from "./FirebaseNotificationEngine";

export default class NotificationService implements NotificationInterface {
  private addressBookRepository: Repository<AddressBookEntity>;
  private addressBookService: AddressBookService;
  private templateMergeService: TemplateMergeService;
  // private notificationEngineMap: Map<string, any>;
  private notificationEngine: NotificationEngine;
  constructor(di: IDI) {
    this.addressBookRepository = di.appTypeOrm
      .getConnection()
      .getRepository(AddressBookEntity);
    this.addressBookService = new AddressBookService(di);
    this.templateMergeService = new TemplateMergeService(di);
    this.notificationEngine = new FirebaseNotificationEngine();
    // this.notificationEngineMap = new Map();
    // this.notificationEngineMap.set("channel", this.notificationEngine);
  }
  public async send(notificationRequest: NotificationRequest): Promise<void> {
    console.log(notificationRequest);
    const addressBookList: AddressBookEntity[] = await this.addressBookService.getAddressBook(
      notificationRequest.userId,
      notificationRequest.channel
    );
    addressBookList.forEach((addressBook) => {
      // this.getNotificationEngine(addressBook.channel).sendNotification(
      this.notificationEngine.sendNotification(
        new NotificationDto(
          addressBook.userId,
          addressBook.channel,
          addressBook.deviceType,
          addressBook.address,
          notificationRequest.subject,
          notificationRequest.message,
          notificationRequest.valueMap
        )
      );
    });
  }

  public async sendWithTemplate(
    notificationRequest: NotificationRequest
  ): Promise<void> {
    console.log(notificationRequest);
    const addressBookList: AddressBookEntity[] = await this.addressBookService.findByUserId(
      notificationRequest.userId
    );
    const mergeResultList: MergeResult[] = await this.templateMergeService.merges(
      notificationRequest.templateCode,
      notificationRequest.valueMap
    );
    console.log("addressBookList ===", addressBookList);
    console.log("mergeResult ===", mergeResultList);

    mergeResultList.forEach((mergeResult) => {
      addressBookList.forEach((addressBook) => {
        console.log("addressBook === ", addressBook);
        // this.getNotificationEngine(addressBook.channel).sendNotification(
        this.notificationEngine.sendNotification(
          new NotificationDto(
            addressBook.userId,
            addressBook.channel,
            addressBook.deviceType,
            addressBook.address,
            notificationRequest.subject,
            notificationRequest.message,
            notificationRequest.valueMap
          )
        );
      });
      // .filter((addressBook) => {
      //   this.equalsIgnoreCase(mergeResult.channel, addressBook.channel);
      // })
    });
  }

  public async sendWithTemplates(
    notificationRequest: NotificationRequest,
    channel: string
  ): Promise<void> {
    console.log(notificationRequest);
    console.log(channel);
    const addressBookList: AddressBookEntity[] = await this.addressBookService.findByUserIdAndChannel(
      notificationRequest.userId,
      channel
    );
    const mergeResult: MergeResult = await this.templateMergeService.merge(
      notificationRequest.templateCode,
      channel,
      notificationRequest.valueMap
    );

    console.log(addressBookList);
    console.log(mergeResult);

    addressBookList.forEach((addressBook) => {
      // this.getNotificationEngine(addressBook.channel).sendNotification(
      this.notificationEngine.sendNotification(
        new NotificationDto(
          addressBook.userId,
          addressBook.channel,
          addressBook.deviceType,
          addressBook.address,
          notificationRequest.subject,
          notificationRequest.message,
          notificationRequest.valueMap
        )
      );
    });
  }

  public async subscribe(addressBook: any): Promise<void> {
    await this.unsubscribeByDeviceId(
      addressBook.deviceType,
      addressBook.channel
    );
    await this.unsubscribeByChannel(addressBook.userId, addressBook.channel);
    const address: string = await this./* getNotificationEngine( */ notificationEngine
      // addressBook.address
      .subscribe(
        addressBook.userId,
        addressBook.deviceType,
        addressBook.deviceId
      );
    addressBook.address = address;
    await this.addressBookService.create(addressBook);
  }

  public async unsubscribe(userId: string): Promise<void> {
    const addressBookList: AddressBookEntity[] = await this.addressBookService.findByUserId(
      userId
    );
    addressBookList.forEach((addressBook) => {
      // this.getNotificationEngine(addressBook.channel).unsubscribe(
      //   addressBook.address
      // );
      this.notificationEngine.unsubscribe(addressBook.address);
      this.addressBookService.deleteByUserId(userId);
    });
  }

  private async unsubscribeByDeviceId(
    deviceId: string,
    channel: string
  ): Promise<void> {
    const addressBook: AddressBookEntity = await this.addressBookService.findByDeviceIdAndChannel(
      deviceId,
      channel
    );
    if (addressBook) {
      // this.getNotificationEngine(addressBook.channel).unsubscribe(
      this.notificationEngine.unsubscribe(addressBook.address);
      this.addressBookRepository.delete(addressBook);
    }
  }

  private async unsubscribeByChannel(
    userId: string,
    channel: string
  ): Promise<void> {
    const addressBookList: AddressBookEntity[] = await this.addressBookService.findByUserIdAndChannel(
      userId,
      channel
    );
    if (addressBookList.length === 0) {
      return;
    }
    addressBookList.forEach((addressBook) => {
      // this.getNotificationEngine(addressBook.channel).unsubscribe(
      this.notificationEngine.unsubscribe(addressBook.address);
    });
    this.addressBookService.deleteByUserIdAndChannel(userId, channel);
  }

  // private getNotificationEngine(channel: string): NotificationEngine {
  //   console.log(this.notificationEngineMap);
  // if (this.notificationEngineMap.ha) {
  // return this.notificationEngineMap.get(channel);
  // } else {
  //   throw new Error(`channel not found ${channel}`);
  // }
  // }

  private equalsIgnoreCase(comp1: string, comp2: string): boolean {
    const isEqual = comp1.toLowerCase() === comp2 ? true : false;
    return isEqual;
  }
}
