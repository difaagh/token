import { TemplateEntity } from "../../entity/template";
import { TemplateChannelEntity } from "../../entity/templateChannel";
import { TemplateParameterEntity } from "../../entity/templateParameter";
import { IDI, TemplateInterface } from "../../interface";
import { Repository } from "typeorm";
import { PageableGetParam } from "../../lib";
import { Pagination } from "../../lib/helper/pagination";
import TemplateChannelService from "./TemplateChannelService";

export default class TemplateService implements TemplateInterface {
  private templateRepository: Repository<TemplateEntity>;
  private templateParameter: Repository<TemplateParameterEntity>;
  // private templateChannelRepository: Repository<TemplateChannelEntity>;
  private templateChannelService: TemplateChannelService;
  constructor(di: IDI) {
    this.templateRepository = di.appTypeOrm
      .getConnection()
      .getRepository(TemplateEntity);
    this.templateParameter = di.appTypeOrm
      .getConnection()
      .getRepository(TemplateParameterEntity);
    // this.templateChannelRepository = di.appTypeOrm
    //   .getConnection()
    //   .getRepository(TemplateChannelEntity);
    this.templateChannelService = new TemplateChannelService(di);
  }

  private async findExits(code: string): Promise<any> {
    return await this.templateRepository.findOne({
      where: {
        code,
        deleteFlag: false,
      },
    });
  }

  public async findByCode(code: string): Promise<any> {
    return await this.templateRepository.find({
      relations: ["templateChannel", "parameter"],
      where: {
        code,
        deleteFlag: false,
      },
    });
  }

  public async findSearch(search: PageableGetParam): Promise<TemplateEntity[]> {
    return await this.templateRepository.find(
      Pagination.paginateOption(search, {
        where: {
          deleteFlag: false,
        },
      })
    );
  }

  public async create(data: any): Promise<void> {
    const exists = await this.findExits(data.code);
    if (exists) {
      throw new Error(`data already exists!`);
    }
    const template = new TemplateEntity();
    template.code = data.code;
    template.description = data.description;
    template.name = data.name;
    template.version = data.version;
    template.idx = data.idx;
    template.deleteFlag = false;
    template.innactiveFlag = true;
    template.createdBy = "admin";
    // logback template channel
    const newtemplateChan = new TemplateChannelEntity();
    newtemplateChan.dateCreated = new Date();
    newtemplateChan.createdBy = "admin";
    newtemplateChan.templateCd = data.code;
    newtemplateChan.channel = data.channel;
    try {
      console.log(newtemplateChan);
      // await this.templateChannelRepository.save(newtemplateChan);
      await this.templateChannelService.create(newtemplateChan);
    } catch (err) {
      console.log(`error channel ${err}`);
      throw new Error(`${err} in channel`);
    }
    // logback parameter
    const param = new TemplateParameterEntity();
    param.templateCode = data.code;
    param.identifier = data.identifier;
    try {
      await this.templateParameter.save(param);
    } catch (e) {
      console.log(e);
      throw new Error(`erorr : ${e}`);
    }
    try {
      // await this.templateRepository.save(template);
      await this.templateRepository.save(template);
    } catch (err) {
      console.log(err);
      throw new Error(`error : ${err}`);
    }
  }

  public async update(newTemplate: any): Promise<void> {
    let isInnactive: boolean;
    const template: TemplateEntity = await this.findExits(newTemplate.code);
    const templateChannelList: TemplateChannelEntity[] = await this.templateChannelService.findByTemplateCode(
      newTemplate.code
    );

    if (template === null) {
      throw new Error(`template not found ${newTemplate.code}`);
    }
    if (
      newTemplate.description !== null ||
      newTemplate.description !== undefined
    ) {
      template.description = newTemplate.description;
    }
    if (newTemplate.name !== null || newTemplate.name !== undefined) {
      template.name = newTemplate.name;
    }
    if (newTemplate.idx !== null || newTemplate.idx !== undefined) {
      template.idx = newTemplate.idx;
    }
    if (
      newTemplate.innactiveFlag !== null ||
      newTemplate.innactiveFlag !== undefined
    ) {
      isInnactive =
        parseInt(newTemplate.innactiveFlag, 10) === 0 ? false : true;
    }
    template.updatedBy = "admin";
    template.lastUpdated = new Date();
    template.innactiveFlag = isInnactive;
    try {
      await this.templateRepository.save(template);
    } catch (e) {
      console.log(e);
    }

    templateChannelList.forEach(async (templateChannel) => {
      const templateChannelEntity: TemplateChannelEntity = await this.templateChannelService.findByTemplateCodeAndChannel(
        template.code,
        templateChannel.channel
      );

      templateChannelEntity.lastUpdated = new Date();
      templateChannelEntity.updatedBy = "admin";
      templateChannelEntity.templates = template;
      templateChannelEntity.content = templateChannel.content;
      templateChannelEntity.subject = templateChannel.subject;
      templateChannelEntity.channel = templateChannel.channel;
      templateChannelEntity.innactiveFlag = isInnactive;

      // await this.templateChannelRepository.save(templateChannelEntity);
      await this.templateChannelService.update(templateChannel);
    });
  }
  // public async getChannelList(): Promise<any> {
  //   const a = await this.templateRepository.find({
  //     relations: ["templateChannel"],
  //   });
  //   return a;
  // }

  public async delete(code: string): Promise<void> {
    const template = await this.findExits(code);
    if (template === undefined) {
      throw new Error(`data not found!`);
    }
    template.deleteFlag = true;
    await this.templateRepository.save(template);
  }
}
