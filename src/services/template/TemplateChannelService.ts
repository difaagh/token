import { TemplateChannelEntity } from "../../entity";
import { IDI, TemplateChannelInterface } from "../../interface";
import { Repository } from "typeorm";

export default class TemplateChannelService
  implements TemplateChannelInterface {
  private templateChannelRepository: Repository<TemplateChannelEntity>;

  constructor(di: IDI) {
    this.templateChannelRepository = di.appTypeOrm
      .getConnection()
      .getRepository(TemplateChannelEntity);
  }
  private async findExits(code: string): Promise<TemplateChannelEntity> {
    console.log("isi code" + code);
    return await this.templateChannelRepository.findOne({
      where: {
        templateCd: code,
        deleteFlag: false,
      },
    });
  }

  public async findByTemplateCodeAndChannel(
    templateCode: string,
    channel: string
  ): Promise<TemplateChannelEntity> {
    return await this.templateChannelRepository.findOne({
      where: {
        templateCd: templateCode,
        channel,
      },
    });
  }

  public async findByTemplateCode(
    templateCode: string
  ): Promise<TemplateChannelEntity[]> {
    return await this.templateChannelRepository.find({
      relations: ["templates"],
    });
  }

  public async getContent(): Promise<TemplateChannelEntity[]> {
    return await this.templateChannelRepository.find({ select: ["content"] });
  }

  public async create(data: TemplateChannelEntity): Promise<void> {
    console.log("isi data" + data);
    const exists = await this.findExits(data.templateCd);
    console.log("isi exists" + exists);
    if (exists) {
      throw new Error(`data already exists!`);
    }
    const newData = new TemplateChannelEntity();
    newData.content = data.content;
    newData.templates = data.templates;
    newData.channel = data.channel;
    newData.templateCd = data.templateCd;
    await this.templateChannelRepository.save(newData);
  }

  public async update(data: any): Promise<void> {
    console.log(data);
  }
}
