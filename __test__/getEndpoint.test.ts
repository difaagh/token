// import * as supertest from "supertest";
import {
  fecthToken,
  fecthTokenConfig,
  fecthtokenTransaction,
  fecthtokenTransactionConfig,
  fecthtokenTransactionAuth,
} from "./getTest";
import { getTestApp } from "./app";

let server: any;
describe("Start server", () => {
  test("It should start the server", async (done) => {
    const app = await getTestApp();
    server = app.listen(3333);
    done();
  });
});
describe("get token", () => {
  test("It Should response the GET method", async (done) => {
    await fecthToken().then((res) => {
      expect(res.status).toBe(200);
      done();
    });
  });
});

describe("Test the getToken path", () => {
  test("It should response the GET method", async (done) => {
    await fecthtokenTransaction().then((res) => {
      expect(res.status).toBe(200);
      done();
    });
  });
});

describe("Test the getTokenConfig path", () => {
  test("It should response the GET method", async (done) => {
    await fecthtokenTransactionAuth().then((res) => {
      expect(res.status).toBe(200);
      done();
    });
  });
});
describe("Test the getTokenTransaction path", () => {
  test("It should response the GET method", async (done) => {
    await fecthtokenTransactionConfig().then((res) => {
      expect(res.status).toBe(200);
      done();
    });
  });
});

describe("Test the getTokenTransactionConfig path", () => {
  test("It should response the GET method", async (done) => {
    await fecthTokenConfig().then((res) => {
      expect(res.status).toBe(200);
      done();
    });
  });
});

describe("Stop the server", () => {
  test("It should stop the server", async (done) => {
    server.close();
    done();
  });
});
