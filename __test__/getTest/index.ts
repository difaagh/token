import axios from "axios";

const API = "http://localhost:3333/api/v1";

export const fecthToken = async () => {
  const url = `${API}/token`;

  return await axios.get(url);
};
fecthToken();
export const fecthTokenConfig = async () => {
  const url = `${API}/tokenconfig`;

  return await axios.get(url);
};
fecthTokenConfig();
export const fecthtokenTransaction = async () => {
  const url = `${API}/tokentransaction`;

  return await axios.get(url);
};
fecthtokenTransaction();
export const fecthtokenTransactionConfig = async () => {
  const url = `${API}/tokentransactionconfig`;

  return await axios.get(url);
};
fecthtokenTransactionConfig();
export const fecthtokenTransactionAuth = async () => {
  const url = `${API}/tokentransactionauth`;

  return await axios.get(url);
};
fecthtokenTransactionAuth();
