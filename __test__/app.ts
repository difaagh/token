import * as Koa from "koa";
import * as cors from "koa2-cors";
import "reflect-metadata";
import { Config } from "../src/lib";
import { AppRoutes } from "../src/app/appRoutes";
import { getDiComponent } from "./di";
import { di, errorCatcher } from "../src/middleware";

export async function getTestApp() {
  const app = new Koa();
  const config = new Config();
  app.use(errorCatcher);
  app.use(di(await getDiComponent(config)));
  app.use(cors);

  const appRoutes = new AppRoutes(false);
  app.use(appRoutes.getRoutes());
  app.use(appRoutes.getAllowedMethods());
  return app;
}
