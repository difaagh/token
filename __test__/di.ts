import { Validator } from "le-validator";
import { PartialResponsify } from "partial-responsify";
import { IDI } from "../src/interface";
import { AppTypeOrm, Config, Helper } from "../src/lib";

export const getDiComponent = async (config: Config): Promise<IDI> => {
  const theDi: IDI = {
    appTypeOrm: null,
    config,
    partialResponsify: new PartialResponsify(),
    helper: new Helper(),
    validator: new Validator(),
  };
  return Promise.resolve(theDi);
};
